#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""
from __future__ import print_function

import argparse
import datetime
import logging
import numpy as np
import os
import tensorflow as tf

import chess
import chess.pgn

from shutil import copyfile

import pdb

from AlphaZeroConfig import AlphaZeroConfig
from ChessGame import ChessGame
from ChessGame import actions
from ChessGame import softmax
from ChessGame import uciActions
from Network import Network

class ChessNetwork(Network):
    def inference(self, images, masks, training: bool = False):
        values, policy_logits = self.sess.run([self.v, self.pi], feed_dict={self.input:images, self.mask:masks, self.isTraining:training})
        return (values, policy_logits)

    def get_weights(self):
        # Returns the weights of this network.
        return []

    def update_weights(self, images, masks, target_values, target_policies, training: bool = True):
        self.sess.run(self.train, feed_dict={self.input:images, self.mask:masks, self.target_value:target_values, self.target_policy:target_policies, self.isTraining:training})

    def predict(self, images, masks, target_values, target_policies, training: bool = False):
        values, policy_logits, loss, grad_norm = self.sess.run([self.v, self.pi, self.loss, self.grad_norm], feed_dict={self.input:images, self.mask:masks, self.target_value:target_values, self.target_policy:target_policies, self.isTraining:training})
        return (values, policy_logits, loss, grad_norm)

    def __del__(self):
        self.sess.close()
        
    def __init__(self, config: AlphaZeroConfig, checkpoint: str = None, step: int = None):
        self.init_elo()
        
        # params
        bn_axis = 3 if config.data_format == 'channels_last' else 1

        # Renaming functions
        Conv2D = tf.layers.conv2d
        Relu = tf.nn.relu
        Tanh = tf.nn.tanh
        BatchNorm = tf.layers.batch_normalization
        Dense = tf.layers.dense
        #Regularizer = tf.contrib.layers.l2_regularizer
        #Flatten = tf.contrib.layers.flatten

        # Neural Net
        tf.reset_default_graph()
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.input = tf.placeholder(tf.float32, shape=(None, 119, 8, 8), name='image')
            self.target_value = tf.placeholder(tf.int8, shape=(None, 1), name='target_value')
            self.target_policy = tf.placeholder(tf.float32, shape=(None, 73, 8, 8), name='target_policy')
            self.mask = tf.placeholder(tf.bool, shape=self.target_policy.shape, name='target_mask')
            self.isTraining = tf.placeholder(tf.bool, name="is_training")

            if config.data_format == 'channels_last':
                x = tf.transpose(self.input, [0, 2, 3, 1])
            else:
                x = self.input

            x = Relu(BatchNorm(Conv2D(inputs=x, filters=config.resnet_filters, kernel_size=3, strides=1, padding='same', data_format=config.data_format, use_bias=False), axis=bn_axis, training=self.isTraining))
            for _ in range(config.resnet_num_blocks):
                x1 = Relu(BatchNorm(Conv2D(inputs=x, filters=config.resnet_filters, kernel_size=3, strides=1, padding='same', data_format=config.data_format, use_bias=False), axis=bn_axis, training=self.isTraining))
                x2 = BatchNorm(Conv2D(inputs=x1, filters=config.resnet_filters, kernel_size=3, strides=1, padding='same', data_format=config.data_format, use_bias=False), axis=bn_axis, training=self.isTraining)
                x += x2
                x = Relu(x)

            conv1 = Relu(BatchNorm(Conv2D(inputs=x, filters=config.resnet_filters, kernel_size=1, strides=1, padding='same', data_format=config.data_format, use_bias=False), axis=bn_axis, training=self.isTraining))
            self.pi = BatchNorm(Conv2D(inputs=conv1, filters=73, kernel_size=1, strides=1, padding='same', data_format=config.data_format, use_bias=False), axis=bn_axis, training=self.isTraining)

            if config.data_format == 'channels_last':
                self.pi = tf.transpose(self.pi, [0, 3, 1, 2])

            conv2 = Relu(BatchNorm(Conv2D(inputs=x, filters=1, kernel_size=1, strides=1, padding='same', data_format=config.data_format, use_bias=False), axis=bn_axis, training=self.isTraining))
            self.v = Tanh(Dense(Relu(Dense(tf.layers.flatten(conv2), units=config.resnet_filters)), units=1))

            pi = tf.boolean_mask(self.pi, self.mask)
            target = tf.boolean_mask(tf.stop_gradient(self.target_policy), self.mask)

            #regularizer = Regularizer(scale=config.weight_decay)
            train_vars = self.graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
            self.loss = tf.reduce_sum(tf.losses.mean_squared_error(predictions=self.v, labels=tf.stop_gradient(self.target_value))
                # self-play should match self.pi with action probabilities of target policy
                + tf.nn.softmax_cross_entropy_with_logits_v2(logits=pi, labels=target)
                #+ tf.contrib.layers.apply_regularization(regularizer, train_vars))
                + config.weight_decay * tf.add_n([tf.nn.l2_loss(weights) for weights in train_vars]))

            optimizer = tf.train.AdamOptimizer()
            #self.train = optimizer.minimize(loss=self.loss)
            #grads_and_vars = optimizer.compute_gradients(loss=self.loss, var_list=vars)
            #clipped_grads_and_vars = [(tf.clip_by_norm(g, config.gradient_clip_val), v) for (g, v) in grads_and_vars]
            #self.train = optimizer.apply_gradients(clipped_grads_and_vars)
            grads_and_vars = optimizer.compute_gradients(loss=self.loss, var_list=train_vars)
            grads, gvs = zip(*grads_and_vars)
            clipped_grads, _ = tf.clip_by_global_norm(grads, config.gradient_clip_val) # gradient clipping
            clipped_grads_and_vars = list(zip(clipped_grads, gvs))
            self.train = optimizer.apply_gradients(clipped_grads_and_vars)
            self.grad_norm = tf.global_norm(grads)

            # tensorboard stuff
            self.add_summary(config)
    
            # saver
            self.saver = tf.train.Saver(self.graph.get_collection(tf.GraphKeys.GLOBAL_VARIABLES), max_to_keep=10, keep_checkpoint_every_n_hours=1)

            # logging
            self.logger = get_logger(config.log_path + 'log.txt') # log file
            if config.dump_graph:
                self.file_writer = tf.summary.FileWriter(config.output_path, self.graph) # tensorboard graph of the NN

        # create tf session
        self.sess = tf.Session(graph=self.graph)
        with tf.Session() as temp_sess:
            temp_sess.run(tf.global_variables_initializer())
        self.sess.run(tf.variables_initializer(self.graph.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)))
        if not checkpoint is None:
            self.load_checkpoint(config.checkpoint_output_path, checkpoint, step)

    def add_summary(self, config:AlphaZeroConfig):
        # extra placeholders to log stuff from python
        self.train_loss_placeholder = tf.placeholder(tf.float32, shape=(), name="train_loss")
        self.dev_loss_placeholder = tf.placeholder(tf.float32, shape=(), name="dev_loss")
        self.test_loss_placeholder = tf.placeholder(tf.float32, shape=(), name="test_loss")
        self.grad_norm_placeholder = tf.placeholder(tf.float32, shape=(), name="grad_norm")
        #self.elo_placeholder = tf.placeholder(tf.float32, shape=(), name="elo_rating")
        #self.delta_elo_placeholder = tf.placeholder(tf.float32, shape=(), name="delta_elo_rating")
        #self.percent_wins_placeholder = tf.placeholder(tf.float32, shape=(), name="percent_wins")
        #self.percent_draw_placeholder = tf.placeholder(tf.float32, shape=(), name="percent_draw")
        #self.percent_loss_placeholder = tf.placeholder(tf.float32, shape=(), name="percent_loss")
        #self.total_wins_placeholder = tf.placeholder(tf.int32, shape=(), name="total_wins")
        #self.total_draw_placeholder = tf.placeholder(tf.int32, shape=(), name="total_draw")
        #self.total_loss_placeholder = tf.placeholder(tf.int32, shape=(), name="total_loss")
        #self.total_games_placeholder = tf.placeholder(tf.int32, shape=(), name="total_games")
    
        # extra summaries from python -> placeholders
        tf.summary.scalar("Train_Loss", self.train_loss_placeholder)
        tf.summary.scalar("Dev_Loss", self.dev_loss_placeholder)
        tf.summary.scalar("Test_Loss", self.test_loss_placeholder)
        tf.summary.scalar("Grad_Norm", self.grad_norm_placeholder)
        #tf.summary.scalar("Elo_Rating", self.elo_placeholder)
        #tf.summary.scalar("Delta_Elo_Rating", self.delta_elo_placeholder)
        #tf.summary.scalar("Percent_Wins", self.percent_wins_placeholder)
        #tf.summary.scalar("Percent_Draws", self.percent_draw_placeholder)
        #tf.summary.scalar("Percent_Loss", self.percent_loss_placeholder)
        #tf.summary.scalar("Total_Wins", self.total_wins_placeholder)
        #tf.summary.scalar("Total_Draws", self.total_loss_placeholder)
        #tf.summary.scalar("Total_Loss", self.total_loss_placeholder)
        #tf.summary.scalar("Total_Loss", self.total_games_placeholder)
                
        self.merged = tf.summary.merge_all()
        
    def init_elo(self):
        self.elo = 1000. # new players get 1000 elo to begin with 
        self.delta_elo = 0
        self.percent_wins = 0.
        self.percent_draw = 0.
        self.percent_loss = 0.
        self.num_games = 0
        self.total_wins = 0
        self.total_draw = 0
        self.total_loss = 0
        self.total_game = 0
        
    def update_elo(self, new_elo, num_wins, num_loss, num_games):
        self.delta_elo = new_elo - self.elo
        self.elo = new_elo
        self.percent_wins = float(num_wins / num_games)
        self.percent_draw = float((num_games - num_wins - num_loss) / num_games)
        self.percent_loss = float(num_loss / num_games)
        self.num_games = num_games
        self.total_wins += num_wins
        self.total_draw += num_games - num_wins - num_loss
        self.total_loss += num_loss
        self.total_game += num_games

    def record_summary(self, step, args, config:AlphaZeroConfig):
        fd = {
            self.isTraining: False,
            self.train_loss_placeholder: args[0],
            self.dev_loss_placeholder: args[1],
            self.test_loss_placeholder: args[2],
            self.grad_norm_placeholder: args[3]
            #self.elo_placeholder: self.elo,
            #self.delta_elo_placeholder : self.delta_elo,
            #self.percent_wins_placeholder : self.percent_wins,
            #self.percent_loss_placeholder : self.percent_loss,
            #self.percent_draw_placeholder : self.percent_draw,
            #self.total_wins_placeholder : self.total_wins,
            #self.total_draw_placeholder : self.total_draw,
            #self.total_loss_placeholder : self.total_loss,
            #self.total_games_placeholder : self.total_game
        }
        summary = self.sess.run(self.merged, feed_dict=fd)
        # tensorboard stuff
        if config.dump_graph:
            self.file_writer.add_summary(summary, step)
        #copyfile("ChessNetwork.py", "{0}/Model{1:05d}.py".format(config.model_output_path, step))
        self.save_checkpoint(config.checkpoint_output_path, "checkpoint", step)
        msg = "{:}:step:{:d}:train_loss:{:f}:dev_loss:{:f}:test_loss:{:f}:grad_norm:{:f}".format(datetime.datetime.now(), step, args[0], args[1], args[2], args[3])
        self.logger.info(msg)

    def save_checkpoint(self, folder: str, filename: str, step: int):
        filePath = os.path.join(folder, "{0}.ckpt".format(filename))
        if not os.path.exists(folder):
            os.mkdir(folder)
        with self.graph.as_default():
            self.saver.save(self.sess, filePath, global_step=step)

    def load_checkpoint(self, folder: str, filename: str, step: int):
        if step is None:
            filePath = os.path.join(folder, "{0}.ckpt".format(filename))
        else:
            filePath = os.path.join(folder, "{0}.ckpt-{1}".format(filename, step))
        with self.graph.as_default():
            self.saver.restore(self.sess, filePath)


def get_logger(filename):
    logger = logging.getLogger('logger')
    logger.setLevel(logging.INFO)
    logging.basicConfig(format='%(message)s', level=logging.INFO)
    handler = logging.FileHandler(filename)
    handler.setLevel(logging.INFO)
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
    logging.getLogger().addHandler(handler)
    return logger

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-cpu', '--cpu', action='store_true')
    args = parser.parse_args()
    config = AlphaZeroConfig()
    config.data_format = 'channels_last' # tensorflow cpu only supports channels_last
    if args.cpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1' # CPU only
    chessGame = ChessGame(chess.Board())
    chessNetwork = ChessNetwork(config)
    image0 = chessGame.make_image(-1)
    mask0 = chessGame.make_mask(-1)
    values, policy_logits = chessNetwork.inference(images=[image0], masks=[mask0])
    print("ChessNetwork value.shape={0} policy_logits.shape={1}".format(values.shape, policy_logits.shape))
    assert values.shape == (1, 1)
    assert policy_logits.shape == (1, 73, 8, 8)
    values, policy_logits = chessNetwork.inference(images=[image0], masks=[mask0])
    chessNetwork.save_checkpoint(config.checkpoint_output_path, "test", 0)
    # Test update_weigthts
    action = actions['e2e4']
    target_policy = np.zeros(chessGame.getActionSize())
    target_policy[action] = 1
    target_value = 1
    images = [image0]
    masks = [mask0]
    target_values = np.reshape(target_value, (1,1))
    target_policies = [target_policy]
    step = 0
    v, p, loss, grad_norm = chessNetwork.predict(images=images, masks=masks, target_values=target_values, target_policies=target_policies)
    (plane, x0, y0) = np.transpose(np.where(mask0))[np.argmax(softmax(p[0][mask0]))]
    predicted_action = uciActions[plane][y0][x0]
    print("{0} p_v = {1}, p_a = {2}, loss = {3}, grad_norm = {4}".format(step, v, predicted_action, loss, grad_norm))
    while predicted_action != 'e2e4': # prediction seems correct and stable when loss reaches < 4, softmax loss will never truly reach 0.
        step += 1
        chessNetwork.update_weights(images=images, masks=masks, target_values=target_values, target_policies=target_policies)
        v, p, loss, grad_norm = chessNetwork.predict(images=images, masks=masks, target_values=target_values, target_policies=target_policies)
        (plane, x0, y0) = np.transpose(np.where(mask0))[np.argmax(softmax(p[0][mask0]))]
        predicted_action = uciActions[plane][y0][x0]
        print("{0} p_v = {1}, p_a = {2}, loss = {3}, grad_norm = {4}".format(step, v, predicted_action, loss, grad_norm))
    # Test saving
    values, policy_logits = chessNetwork.inference(images=[image0], masks=[mask0])
    chessNetwork2 = ChessNetwork(config, "test", 0)
    values2, policy_logits2 = chessNetwork2.inference(images=[chessGame.make_image(-1)], masks=[chessGame.make_mask(-1)])
    assert values2.shape == (1, 1)
    assert policy_logits2.shape == (1, 73, 8, 8)
    assert values != values2
    assert (policy_logits != policy_logits2).any()
    chessNetwork.save_checkpoint(config.checkpoint_output_path, "test2", 0)
    chessNetwork = ChessNetwork(config, "test", 0)
    values, policy_logits = chessNetwork.inference(images=[image0], masks=[mask0])
    values2, policy_logits2 = chessNetwork2.inference(images=[image0], masks=[mask0])
    assert values == values2
    assert (policy_logits == policy_logits2).all()
    chessNetwork = ChessNetwork(config, "test2", 0)
    values, policy_logits = chessNetwork.inference(images=[image0], masks=[mask0])
    values2, policy_logits2 = chessNetwork2.inference(images=[image0], masks=[mask0])
    assert values2.shape == (1, 1)
    assert policy_logits2.shape == (1, 73, 8, 8)
    assert values != values2
    assert (policy_logits != policy_logits2).any()
    print("ChessNetwork: passed")