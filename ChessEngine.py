#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""
from __future__ import print_function

import numpy as np

from ChessGame import ChessGame

def calcExpScore(ratingA,ratingB):
  delta = ratingB - ratingA
  expectedScore = 1/(1 + np.power(10,delta/400))
  return expectedScore

def EloUpdate(Elo, actualScore, expectedScore, K = 32):
  Elo = Elo + K*(actualScore - expectedScore)
  return Elo

class ChessEngine(object):
  def __init__(self, elo: int):
    self.elo = elo

  def name(self):
    pass

  def play(self, chessGame: ChessGame):
    pass

  def update(self, opponent_elo: int, result: int):
    expectedScore = calcExpScore(self.elo, opponent_elo)
    self.elo = EloUpdate(self.elo, result, expectedScore)

  def quit(self):
    pass