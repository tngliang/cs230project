from __future__ import print_function

import numpy as np

import chess
import h5py
import os
import pdb

from AlphaZeroConfig import AlphaZeroConfig
from ChessGame import ChessGame

class ReplayBuffer(object):

  def __init__(self, config: AlphaZeroConfig, reload: bool = False):
    self.config = config
    self.window_size = config.window_size
    self.batch_size = config.batch_size
    self.buffer = []

    gamesDir = config.game_output_path
    self.step = 0
    if not os.path.exists(gamesDir):
      os.makedirs(gamesDir)
    for item in os.listdir(gamesDir):
      filePath = os.path.join(gamesDir, item)
      if os.path.isfile(filePath):
        prefix_index = item.find('selfplay')
        suffix_index = item.find(".h5")
        if prefix_index >= 0 and suffix_index > prefix_index:
          step = int(item[prefix_index+8:suffix_index])
          if step >= self.step:
            self.step = step
    self.step += 1
    self.h5py_file = h5py.File(os.path.join(gamesDir, "selfplay{:d}.h5".format(self.step)), 'w')
    self.h5py_games = self.h5py_file.create_group("games")
    self.count = 0

  def __del__(self):
    self.h5py_file.close()

  def save_game(self, game):
    if len(self.buffer) > self.window_size:
      self.buffer.pop(0)
    self.buffer.append(game)
    game.saveh5py(self.h5py_games.create_group(str(self.count)))
    self.count += 1

  def sample_batch(self):
    # Sample uniformly across positions.
    move_sum = float(sum(len(g.history) for g in self.buffer))
    games = np.random.choice(
        self.buffer,
        size=self.batch_size,
        p=[len(g.history) / move_sum for g in self.buffer])
    game_pos = [(g, np.random.randint(len(g.history))) for g in games]
    #return [(g.make_image(i), g.make_target(i)) for (g, i) in game_pos]
    return ([g.make_image(i) for (g, i) in game_pos], [g.make_target(i) for (g, i) in game_pos], [g.make_mask(i) for (g, i) in game_pos])

  def sample_moves(self):
    move_sum = sum(len(g.history) for g in self.buffer)
    num_sampled = 0
    while num_sampled < move_sum:
      games = np.random.choice(
          self.buffer,
          size=self.batch_size,
          p=[len(g.history) / float(move_sum) for g in self.buffer])
      game_pos = [(g, np.random.randint(len(g.history))) for g in games]
      yield ([g.make_image(i) for (g, i) in game_pos], [g.make_target(i) for (g, i) in game_pos], [g.make_mask(i) for (g, i) in game_pos])
      num_sampled += self.batch_size

    