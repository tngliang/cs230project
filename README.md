Installation Instruction for machine with NVIDIA GPU (assuming CUDA 9.0 and cuDNN 7.4)

1. Download and install python 3.6.4 from https://www.python.org/downloads/release/python-364/, check option to install pip
2. Run pip install virtualenv
3. Create a virtual environment by running virtualenv .env
4. Activate virtual environment by running .env\scripts\activate.bat
5. Downgrade pip to 18.1 (because pyinstaller doesn't work with 19.1) by running pip install pip==18.1
6. Install pytorch (CUDA 9.0) manually by running pip3 install https://download.pytorch.org/whl/cu90/torch-1.0.0-cp36-cp36m-win_amd64.whl
7. Install all other requirements by running pip install -r requirements.txt

The Chess Game environment is implemented in ChessGame.py.  It contains boardToState method to convert Python Chess Boards to AlphaZero state space, createUciActionMap to map between python chess Algebraic Notation and AlphaZero action space.  To see an example of integration of the ChessGame environment please view NeuralChessZero.py.  To run unit tests please run python ChessGame.py.

The Artificial Neural Network is implemented in LiaCha_Model.py.  It is also consumed by NeuralChessZero.py to generate best moves.

NeuralChessZero.py implements a rudimentary chess engine that can interface with any UCI compliant chess GUIs (eg Deep Junior (https://shop.chessbase.com/en/products/deep_junior_138) or Lucas Chess (https://lucaschess.pythonanywhere.com/).  It currently generate action probabilities and pass state and actionProb to ChessGame's findBestMove to generate the next move.  It is currently playable.  To interact with it with python NeuralChessZero.py, and type uci<enter>.  However please use pyinstaller to compile to exe because most UCI engines require executable not raw python files.  To compile a UCI compatible Chess Engine run pyinstaller NeuralChessZero.py.  This should create dist\NeuralChessZero\NeuroChessZero.exe, which can be imported into a UCI compliant Chess GUI  for engine tournament or human play.

To test UCI engine manually simply run python NeuralChessZero.py.  A sample log of commands and response can be found in NeuralChessZero.log.   NeuralChessZero.py is currently connected to the LiaCha_Model.py neural network for bestmove generation using the Policy Head.

pgnwriter_example.py contains an example of engine to engine self-play or tournament with the option to write the generated game in PGN text format. To load and view generated PGN games use free http://www.wmlsoftware.com/chesspad.html.

+Currently NeuralChessZero.py simply proxies to an existing Chess Engine for bestmove generation.  To test with stockfish 10 download from https://stockfishchess.org/download/ and place Stockfish_10_x64.exe next to NeuralChessZero.py or NeuralChessZero.exe respectively.  This requirement will be removed when the project is complete.

To perform self-play game generation run AlphaZero.py -s
To perform training on game already generated and stored under data/train, data/dev and data/test, and continue training using latest network checkpoint stored under results/checkpoints, run AlphaZero.py -t
To perform preprocessing of game pgn and convert them to .h5 files, run AlphaZero.py -l <pgn file dir>
To perform chess tournaments evaluation between engines run ChessTournament -p1 <NeuralChessZero|Stockfish> -p2 <NeuralChessZero|Stockfish> -c1 <checkpoint number> -c2 <checkpoint number> -n <number of games> -s <suffix to append to pgn result file>
