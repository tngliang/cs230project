import chess
import chess.pgn
import chess.engine
import numpy as np
import datetime

from LiaCha_model import LiaCha
import torch
import torch.nn.utils

from ChessGame import ChessGame

# calculates expected score for player A
def calcExpScoreA(ratingA,ratingB):
  delta = ratingB - ratingA
  expectedScore = 1/(1 + np.power(10,delta/400))
  return expectedScore

def EloUpdate(Elo, actualScore, expectedScore, K = 32):
  Elo = Elo + K*(actualScore - expectedScore)
  return Elo

def main(): 
  # loads stockfish engine
  engine = chess.engine.SimpleEngine.popen_uci("Stockfish_10_x64")
  exporter = chess.pgn.StringExporter(headers=True, variations=True, comments=True)

  # initializes PGN (Portable Game Notation) file
  pgnfile = open("testgame.pgn", "w")

  # initializes dictionary for keep track of win/draw/losses
  scoreTable = {'White':0, 'Black':0, 'Draw':0}
  
  # Current this is hardcoded. Will likely save elorating in a cfg file separately
  LiCha_elo = 0
  args_dict = {"EloRating": LiCha_elo}

  # Initialize model
  model = LiaCha(**args_dict)
  model.EloRating = 0

  # Initialize elo scores
  # http://legacy-tcec.chessdom.com/archive.php list all current chess engine ratings (bottom right)
  EloRating = {'Stockfish':3232, 'LiCha':model.EloRating}
  players = ["Stockfish 10 (1)", "Stockfish 10 (2)"]
  playerelos = [str(EloRating['Stockfish']), str(EloRating['LiCha'])]

  # Game parameters
  i = 0
  numRounds = 10

  ### STARTS GAME HERE!!!!! ###
  for Round in range(numRounds):
    j = (i + 1) % len(players)
    board = chess.Board()
    # CUSTOMGAME = ChessGame(chess.Board())
    while not board.is_game_over():
      result = engine.play(board, chess.engine.Limit(time=None))
      board.push(result.move)
    
    # logs PGN (Portable Game Notation) into pgnfile
    game = chess.pgn.Game.from_board(board)
    game.headers.update({
      "Event" : "Stockfish 10 v Stockfish 10",
      "Site": "Stanford",
      "Date": datetime.datetime.now().strftime("%Y-%m-%d"),
      "Round": Round,
      "White" : players[i],
      "WhiteElo" : playerelos[i], # TODO need to update this later
      "Black" : players[j],
      "BlackElo" : playerelos[j] # TODO need to update this later
    })
    if board.is_game_over():
      game.headers["Result"] = board.result(claim_draw=True)
    # print(game.headers)
    pgnfile.write(game.accept(exporter))
    pgnfile.write("\n\n")
    i = (i + 1) % len(players)

    # logs score results; Assume white is our engine LiaCha
    gameResult = board.result() #returns white - black: 1-0, 0-1 or 1/2-1/2 (type Str)

    if gameResult == '1-0':
      scoreTable["White"] += 1
      actualScore = 1
    elif gameResult == '0-1':
      scoreTable["Black"] += 1
      actualScore = 0
    else: #Assumes a finished game
      scoreTable["Draw"] += 1  
      actualScore = 0.5
    print(scoreTable)

    # Update Elo Rating - keep stockfish elo rating constant...
    expectedScore = calcExpScoreA(ratingA = EloRating['LiCha'], ratingB = EloRating['Stockfish'])
    EloRating['LiCha'] = EloUpdate(EloRating['LiCha'], actualScore, expectedScore)
    model.EloRating = EloRating['LiCha']
    print(model.EloRating)

  pgnfile.close()
  engine.quit()

if __name__ == '__main__':
    main()