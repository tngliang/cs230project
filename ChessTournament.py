#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""
from __future__ import print_function

import argparse
import chess
import chess.pgn
import datetime
import os

import pdb

from AlphaZeroConfig import AlphaZeroConfig
from ChessGame import ChessGame
from ChessEngine import ChessEngine
from ChessNetwork import ChessNetwork
from NeuralChessZeroEngine import NeuralChessZeroChessEngine
from StockfishChessEngine import StockfishChessEngine

class ChessTournament(object):
  def __init__(self, num_games: int):
    self.num_games = num_games

  def Run(self, step: int, player1: ChessEngine, player2: ChessEngine, config: AlphaZeroConfig):
    exporter = chess.pgn.StringExporter(headers=True, variations=True, comments=True)

    try:
        pgnString = None
        whiteColor = 1
        for i in range(self.num_games):
            chessGame = ChessGame(chess.Board())
            player2white = i % 2 == whiteColor
            white = player2.name if player2white else player1.name
            black = player1.name if player2white else player2.name
            print("Game({0}) white={1} black={2}".format(i, white, black))
            while not chessGame.getGameEnded():
                if i % 2 == chessGame.to_play():
                    player2.play(chessGame)
                else:
                    player1.play(chessGame)
            whiteElo = round(player2.elo if player2white else player1.elo, 0)
            blackElo = round(player1.elo if player2white else player2.elo, 0)
            whiteScore = chessGame.getWhiteScore()
            blackScore = 1 - whiteScore
            resultString = chessGame.getGameResult()
            now = datetime.datetime.now()
            game = chess.pgn.Game.from_board(chessGame.board)
            game.headers.update({
                "Event" : "{0} v {1} @ step {2}".format(white, black, step),
                "Site": "Stanford",
                "Date": now.strftime("%Y-%m-%d"),
                "Round": i,
                "White" : white,
                "WhiteElo" : str(whiteElo),
                "Black" : black,
                "BlackElo" : str(blackElo),
                "Result" : resultString
            })
            pgnString = game.accept(exporter) # export game pgn
            player1_elo = player1.elo
            player2_elo = player2.elo
            player1.update(player2_elo, blackScore if player2white else whiteScore)
            player2.update(player1_elo, whiteScore if player2white else blackScore)
            print("{0} Game({1}) white={2} black={3} result={4}, white={5}, black={6}".format(
                now, i, white, black, resultString, whiteScore, blackScore))
    finally:
        if not pgnString is None:
            if not os.path.exists(config.game_output_path):
                os.makedirs(config.game_output_path)
            pgnfile = open(config.game_output_path + "ChessTournament{0}.pgn".format(step), "w")
            pgnfile.write(pgnString)
            pgnfile.close()

def main(args):
    config = AlphaZeroConfig()
    if args.cpu:
        config.data_format = 'channels_last' # tensorflow cpu only supports channels_last
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1' # CPU only
    if args.num_games is None:
        args.num_games = 24 # 24 rounds
    if args.step is None:
        args.step = -1
    tournament = ChessTournament(args.num_games)
    if args.player1 is None or args.player1 == 'NeuralChessZero':
        if args.checkpoint1 is None:
            player1 = NeuralChessZeroChessEngine(config, ChessNetwork(config), 0)
        else:
            step = (int(args.checkpoint1))
            player1 = NeuralChessZeroChessEngine(config, ChessNetwork(config, "checkpoint", step), step)
    else:
        player1 = StockfishChessEngine()
    if args.player2 is None or args.player2 == 'Stockfish':
        player2 = StockfishChessEngine()
    else:
        if args.checkpoint2 is None:
            player2 = NeuralChessZeroChessEngine(config, ChessNetwork(config), 0)
        else:
            step = (int(args.checkpoint2))
            player2 = NeuralChessZeroChessEngine(config, ChessNetwork(config, "checkpoint", step), step)
    tournament.Run(args.step, player1, player2, config)
    print(player1.elo)
    print(player2.elo)
    player1.quit()
    player2.quit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-cpu', '--cpu', action='store_true')
    parser.add_argument('-p1', '--player1', help='engine name for player1')
    parser.add_argument('-p2', '--player2', help='engine name for player2')
    parser.add_argument('-c1', '--checkpoint1', help='checkpoint for player1')
    parser.add_argument('-c2', '--checkpoint2', help='checkpoint for player2')
    parser.add_argument('-n', '--num_games', type=int)
    parser.add_argument('-s', '--step', type=int)
    args = parser.parse_args()
    main(args)
