from __future__ import print_function

import os

from AlphaZeroConfig import AlphaZeroConfig
from Network import Network
from ChessEngine import ChessEngine
from ChessNetwork import ChessNetwork

class SharedStorage(object):

  def __init__(self, config: AlphaZeroConfig):
    self._networks = {}
    self.config = config

  def latest_network(self) -> Network:
    if self._networks:
      return self._networks[max(iter(self._networks.keys()))]
    else:
      return self.make_uniform_network()  # policy -> uniform, value -> 0.5

  def save_network(self, step: int, network: Network):
    self._networks[step] = network
    
  def make_uniform_network(self):
    return ChessNetwork(self.config)
