class AlphaZeroConfig(object):

  def __init__(self):
    ### Self-Play
    self.num_actors = 5000

    self.num_sampling_moves = 30
    self.max_moves = 512  # for chess and shogi, 722 for Go.
    self.num_simulations = 800

    # Root prior exploration noise.
    self.root_dirichlet_alpha = 0.3  # for chess, 0.03 for Go and 0.15 for shogi.
    self.root_exploration_fraction = 0.25

    # UCB formula
    self.pb_c_base = 19652
    self.pb_c_init = 1.25

    ### Training
    self.training_steps = int(700e3)
    # self.checkpoint_interval = int(1e3)
    self.checkpoint_interval = int(1e1)

    self.window_size = int(1e6)
    #self.batch_size = 4096
    #self.batch_size = 64 # ok when self.resnet_num_blocks = 1
    #self.batch_size = 256 # ok when self.resnet_num_blocks = 2
    self.batch_size = 64
    
    #self.weight_decay = 1e-4
    self.weight_decay=2e+2
    self.momentum = 0.9
    # Schedule for chess and shogi, Go starts at 2e-2 immediately.
    #self.learning_rate_schedule = {
    #    0: 2e-1,
    #    100e3: 2e-2,
    #    300e3: 2e-3,
    #    500e3: 2e-4
    #}
    self.learning_rate_schedule = {
        0: 2e-1,
        1000: 2e-2,
        300e3: 2e-3,
        500e3: 2e-4
    }
    """
    CS230 2019-Winter - Final Project: Recreating AlphaZero
    Mark Chang <mrkchang@stanford.edu>
    Ting Liang <tngliang@stanford.edu>
    """
    self.dump_graph = False
    self.resnet_filters = 256
    self.resnet_num_blocks = 19
    #self.resnet_num_blocks = 1
    self.gradient_clip_val = 1e-3
    self.eval_num_games = 10
    self.max_games_to_load = 1000
    self.num_steps_per_games = 100
    self.data_format = 'channels_last'
    self.output_path = "results/"
    self.checkpoint_output_path = self.output_path + "checkpoints/"
    self.game_output_path = self.output_path + "games/"
    self.log_path = self.output_path + "logs/"
    self.data_path = "data/"