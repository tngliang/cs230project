import torch
import torch.nn.utils
import numpy as np
from LiaCha_model import LiaCha
from ChessGame import ChessGame
import chess
import pdb
import time
import util
from ChessGame import actions as actionsMAP

# from scipy.special import softmax

class Node(object):
  def __init__(self, P: float):
    # AlphaZero:
    # N - visit count
    # W - total action value
    # Q - mean action value. the expected reward for taking a from s
    # P - prior probability of selection of selecting a in s according model
    self.N = 0
    self.W = 0
    self.Q = 0
    self.P = P
    self.Prediction = 0
    self.children = {} # looks more like action, node pair since action is selected and leads to node...

class MCTS(object):
  # performs Monte Carlo Tree Search (MCTS) with the current model as is 
  def __init__(self, game, model):
    self.game = game
    self.model = model
    self.root = Node(P = 0)
    self.trainingSet = None

    # for UCB function
    self.pb_c_base = 19652 
    self.pb_c_init = 1.25

    # Dirichlet noise
    self.dirNoise = 0.3

  def run(self, simulations = 800, viewGame = False):
  # plays n simulations to generate one giant tree
  # returns simulation count training vector
    sleepy = 0.1
    root = self.root
    NodePath = [root]
    NodePath, score = self.search(self.game, NodePath)
    self.backprop(NodePath, score)
    self.addDirichletNoise(root, noise = self.dirNoise)

    self.trainingSet = []
    
    mainGame = ChessGame(chess.Board())
    while mainGame.getGameEnded() == 0:
      if viewGame == True:
        print("main board")
        print("*"*50)
        time.sleep(sleepy)
        print(mainGame.board)
        print("*"*50)


      # Creates one tree of n simulations from specific node
      treeGame = ChessGame(chess.Board(fen = mainGame.toString()))
      for count in range(simulations):
        print('simulation:' + str(count + 1))
        print('current node path length:' + str(len(NodePath)))
        # treeGame = ChessGame(chess.Board(fen = mainGame.toString()))
        NodePath, score = self.search(treeGame, NodePath)
        self.backprop(NodePath, score)
        
        # Turn flag on for wanting to visualize the progress
        if viewGame == True:
          print("tree board")
          print("*"*50)
          time.sleep(sleepy)
          print(treeGame.board)
          print("*"*50)

      if viewGame == True:
        print("main board")
        print("*"*50)
        time.sleep(sleepy)
        print(mainGame.board)
        print("*"*50)

      # generates search prob vec for training
      parentNode = NodePath[0]
      searchProb = self.generateSearchProb(parentNode, mainGame)
      self.trainingSet.append((searchProb, mainGame.state))


      # Makes a move based on searchProb
      action = mainGame.findBestMove(mainGame.state, searchProb)
      mainGame.makeMove(action) # takes a step in the game
      NodePath = [parentNode.children[action]] #re-itialize the node path

    # trainingSet should include game states, move probabilities vector pi at every step down the tree
    # TODO: understand if the outcome should be multiplied by 1 or not
    outcome = self.game.getScore()
    
    return self.trainingSet, outcome

  def search(self, game, NodePath):
  # goal is for this function to perform a single search down the Monte Carlos Search Tree
  # how we implemented it makes sure that it goes all the way down to the terminal state.
    
    state = game.boardToState()
    currNode = NodePath[-1]

    if game.getGameEnded() == 1:
      result = util.interpretResult(game.board.result(claim_draw = True))
      return NodePath, -result

    # Case: if this is not a leaf note, find a leaf node
    if currNode.children:
      # Apply UCB function to find next node
      action, intermediateNode = self.maxUCBScore(currNode)
      game.makeMove(action)
      NodePath.append(intermediateNode)
      return self.search(game, NodePath) # recursion

    # Case: If this is a leaf node
    else:
      # Interesting note here is that MCTS with AlphaZero does not have a rollout implementation
      # Instead, it returns the expected value from the neural network.
      actionProbs, prediction = self.model(torch.tensor(state, dtype = torch.float).unsqueeze(0))
      currNode.Prediction = prediction
      actionProbs = torch.squeeze(actionProbs, 0).detach().numpy()
      ActionAndMoveProbs = game.getValidActionAndMoveProb(state, actionProbs)

      # create all the relevant nodes.
      for ActionAndMoveProb in ActionAndMoveProbs:
        action, moveProb = ActionAndMoveProb
        currNode.children[action] = Node(P = moveProb)
      
      return NodePath, -prediction

  def maxUCBScore(self, Parent): 
    ucb_score_max = 0
    N = Parent.N
    maxAction = None
    maxNode = None
    for action, Node in Parent.children.items():
      # C: Exploration rate
      C = np.log((1 + N + self.pb_c_base)/self.pb_c_base) + self.pb_c_init
      U = C * Node.P * np.sqrt(N) / (1 + N)
      ucb_score = Node.Q + U
      if ucb_score > ucb_score_max:
        ucb_score_max = ucb_score
        maxAction = action
        maxNode = Node

    if maxAction is None or maxNode is None:
      print(ucb_score)
    return maxAction, maxNode
      
  def backprop(self, nodeList, score):
    for node in nodeList:
      node.N += 1
      node.W += score
      node.Q = node.W / node.N
    return None

  def addDirichletNoise(self, root, noise = 0.3):
    actions = root.children.keys()
    dirichletNoise = np.random.dirichlet(alpha = [noise]*len(actions))
    for action, noise in zip(actions,dirichletNoise):
      root.children[action].P = root.children[action].P*noise

  def searchProb(self, root):
    Nvec = [child.N for child in root.children.iteritems()]
    return Nvec/sum(Nvec)

  def generateSearchProb(self, parentNode, game):
  # generates 1 searchProb of dim 1x73x8x8
    searchProb = np.zeros((73,8,8))

    for action, childNode in parentNode.children.items():
      (plane, x0, y0) = actionsMAP[action]
      searchProb[plane, x0, y0] = childNode.N
      # print("move: " + action + " and visitcount: " + str(childNode.N))

    searchProb /= np.sum(searchProb)
    # searchProb = np.expand_dims(searchProb, axis = 0)
    return searchProb

  def recommendBestMove(self, board, simulations = 800):
        # Creates one tree of n simulations from specific node
      root = self.root
      NodePath = [root]
      NodePath, score = self.search(self.game, NodePath)
      self.backprop(NodePath, score)
      self.addDirichletNoise(root, noise = self.dirNoise)
      
      treeGame = ChessGame(chess.Board(fen = board.fen()))
      # print(treeGame.board)
      for count in range(simulations):
        NodePath, score = self.search(treeGame, NodePath)
        self.backprop(NodePath, score)
        
      # generates search prob vec for training
      parentNode = NodePath[0]
      searchProb = self.generateSearchProb(parentNode, treeGame)

      # Makes a move based on searchProb
      action = treeGame.findBestMoveMCTS(ChessGame(chess.Board(fen = board.fen())).boardToState(), searchProb)
      return action, root
  

if __name__=="__main__":
  print("enter if you want to play 1 game")
  chessGame = ChessGame(chess.Board())
  LiaCha = LiaCha()
  saveTheTrees = MCTS(game = chessGame, model = LiaCha)
  trainingExample = saveTheTrees.run(simulations = 5, viewGame = True)
  print(trainingExample)



  
  
  
  
  """
  # # rollout actually is not used in AlphaZero MCTS
  # def rollout(self, fen):
  #   # create a newboard and iterate to completion
  #   board = chess.Board(fen = fen)
  #   newGame = ChessGame(board)
  #   while newGame.getGameEnded() == 0:
  #     state = newGame.boardToState()
  #     actionSize = newGame.getActionSize()
  #     actionProbs = softmax(np.random.randn(actionSize[0], actionSize[1], actionSize[2])) #generate random move
  #     result = newGame.findBestMove(state, actionProbs)
  #     state = newGame.makeMove(result)
  #   score = newGame.getScore()
  #   return score
"""