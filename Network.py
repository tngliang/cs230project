#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""

class Network(object):

  def inference(self, image, masks, training: bool = False):
    return (-1, {})  # Value, Policy

  def get_weights(self):
    # Returns the weights of this network.
    return []

  def save(self, filename):
    pass

  def update_weights(self, image, masks, target_values, target_policies, training: bool = True):
    pass

  def predict(self, images, masks, target_values, target_policies, training: bool = False):
    pass

  def record_summary(self, step, metrics, config):
    pass