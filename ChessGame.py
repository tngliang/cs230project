#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""

from __future__ import print_function

import numpy as np
import collections

import chess
import chess.pgn
import datetime

from Game import Game

import traceback
import pdb

def softmax(z):
    e_z = np.exp(z)
    e_z_s = np.sum(e_z)
    return e_z / e_z_s

# Alpha zero action space consists of 8 clockwise directions: N, NE, E, SE, S, SW, W, NW 
n = 8
QueenMoves = np.array([[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]])
KnightMoves = np.array([[1, 2], [2, -1], [2, 1], [1, -2], [-1, -2], [-2, -1], [-2, 1], [-1, 2]])
PawnMoves = np.array([[0, 1], [1, 1], [-1, 1]])
    
def toUciMove(x0, y0, x1, y1, promotion):
    return chr(ord('a') + x0) + str(y0 + 1) + chr(ord('a') + x1) + str(y1 + 1) + promotion

def createUciActionMap():
    # Alpha Zero Action space consists of 73 planes of 8*8 starting square
    actions = {}
    uciActions = np.full(shape=(73, n, n), fill_value=None)
    # 56 planes for 'Queen' moves in respective clockwise directions of N, NE, E, SE, S, SW, W, NW for distance between 1..7 steps
    plane = 0
    for direction in QueenMoves:
        # 1..7 steps starting at the selected 8*8 square
        for steps in range(1, n):
            for y0 in range(n):
                for x0 in range(n):
                    [x1, y1] = [x0, y0] + direction * steps
                    if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                        uci_move = toUciMove(x0, y0, x1, y1, '')
                        actions[uci_move] = (plane, x0, y0)
                        uciActions[plane][y0][x0] = uci_move
                        if y0 == n - 2 and y1 == n - 1:
                            uci_promo_move = toUciMove(x0, y0, x1, y1, 'q')
                            actions[uci_promo_move] = (plane, x0, y0)
                        if y0 == 1 and y1 == 0:
                            uci_promo_move = toUciMove(x0, y0, x1, y1, 'q')
                            actions[uci_promo_move] = (plane, x0, y0)
            plane += 1
    # 8 planes for 'Knight' moves
    for direction in KnightMoves:
        for x0 in range(n):
            for y0 in range(n):
                [x1, y1] = [x0, y0] + direction
                if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                    uci_move = toUciMove(x0, y0, x1, y1, '')
                    actions[uci_move] = (plane, x0, y0)
                    uciActions[plane][y0][x0] = uci_move
        plane += 1
    # 3 planes for underpromotion to rook
    for direction in PawnMoves:
        for x0 in range(n):
            y0 = 6
            [x1, y1] = [x0, y0] + direction
            if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                uci_move = toUciMove(x0, y0, x1, y1, 'r')
                actions[uci_move] = (plane, x0, y0) 
                uciActions[plane][y0][x0] = uci_move
            y0 = 1
            reverse_direction = np.array([direction[0], -direction[1]])
            [x1, y1] = [x0, y0] + reverse_direction
            if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                uci_move = toUciMove(x0, y0, x1, y1, 'r')
                actions[uci_move] = (plane, x0, y0)
                uciActions[plane][y0][x0] = uci_move
        plane += 1
    # 3 planes for underpromotion to bishop
    for direction in PawnMoves:
        for x0 in range(n):
            y0 = 6
            [x1, y1] = [x0, y0] + direction
            if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                uci_move = toUciMove(x0, y0, x1, y1, 'b')
                actions[uci_move] = (plane, x0, y0)
                uciActions[plane][y0][x0] = uci_move
            y0 = 1
            reverse_direction = np.array([direction[0], -direction[1]])
            [x1, y1] = [x0, y0] + reverse_direction
            if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                uci_move = toUciMove(x0, y0, x1, y1, 'b')
                actions[uci_move] = (plane, x0, y0) 
                uciActions[plane][y0][x0] = uci_move
        plane += 1
    # 3 planes for underpromotion to knight
    for direction in PawnMoves:
        for x0 in range(n):
            y0 = 6
            [x1, y1] = [x0, y0] + direction
            if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                uci_move = toUciMove(x0, y0, x1, y1, 'n')
                actions[uci_move] = (plane, x0, y0)
                uciActions[plane][y0][x0] = uci_move
            y0 = 1
            reverse_direction = np.array([direction[0], -direction[1]])
            [x1, y1] = [x0, y0] + reverse_direction
            if 0 <= x1 and x1 < n and 0 <= y1 and y1 < n:
                uci_move = toUciMove(x0, y0, x1, y1, 'n')
                actions[uci_move] = (plane, x0, y0)
                uciActions[plane][y0][x0] = uci_move
        plane += 1
    # ============================== Total 56 + 8 + 9 = 73 planes
    assert(plane == 73)
    return actions, uciActions

actions, uciActions = createUciActionMap()
BoardToState = {}

class ChessGame(Game):
    def terminal(self):
        return self.getGameEnded()

    def terminal_value(self, to_play):
        # Game specific value.
        return self.getScore(to_play)

    def legal_actions(self):
        # Game specific calculation of legal actions.
        return self.getValidActions(self.boardToState())

    def clone(self):
        #clone = ChessGame(chess.Board())
        #for action in self.history:
        #    clone.apply(action)
        cloneBoard = chess.Board(fen = self.board.fen())
        clone = ChessGame(cloneBoard)
        clone.boards = self.boards
        clone.boards[0] = cloneBoard
        return clone

    def apply(self, action, store: bool = True):
        (plane, x0, y0) = action
        uci_action = uciActions[plane][y0][x0]
        move = chess.Move.from_uci(uci_action)
        if (plane < 56 and y0 == 6 and chess.square_rank(move.to_square) == 7 and self.board.piece_at(move.from_square).symbol() == 'P'):
          uci_action += 'q'
        if (plane < 56 and y0 == 1 and chess.square_rank(move.to_square) == 0 and self.board.piece_at(move.from_square).symbol() == 'p'):
          uci_action += 'q'
        self.makeMove(uci_action)
        state = self.boardToState()
        self.states.append(state) # store board state
        self.legals.append(self.getValidActionsMask(state)) # store legal actions
        super().apply(action) # commit to super class history also
        
    def store_search_statistics(self, root):
        sum_visits = sum(child.visit_count for child in root.children.values())
        #self.child_visits.append([
        #    root.children[a].visit_count / sum_visits if a in root.children else 0
        #    for a in np.ndindex(self.getActionSize())
        #])
        child_visits = np.zeros(self.getActionSize())
        for a in root.children:
            child_visits[a] = root.children[a].visit_count / sum_visits
        self.child_visits.append(child_visits)

    def make_image(self, state_index: int):
        if (state_index == -1):
            return self.boardToState()
        else:
            #clone = ChessGame(chess.Board())
            #for i in range(0, min(len(self.history), state_index)):
            #    clone.apply(self.history[i])
            #return clone.boardToState()
            return self.states[state_index]
    
    def make_mask(self, state_index: int):
        if (state_index == -1):
            return self.getValidActionsMask(self.boardToState())
        else:
            return self.legals[state_index]

    def make_target(self, state_index: int):
        target_value = self.terminal_value(state_index % 2)
        if state_index < len(self.child_visits):
            target_policy = self.child_visits[state_index]
        else:
            target_policy = np.zeros(self.getActionSize())
            target_policy[self.history[state_index]] = 1 # one-hot historic action as target (for supervised-learning)
        return (target_value, target_policy)

    def to_play(self):
        return 1 - len(self.history) % 2 # 1 = white, 0 = black

    @staticmethod
    def load(npz_file):
        try:
            npzfile = np.load(npz_file)
            game = ChessGame(chess.Board())
            game.states = npzfile['states']
            game.legals = npzfile['legals']
            game.history = npzfile['history']
            game.child_visits = npzfile['child_visits']
            game.terminated, game.result, game.score, game.whiteScore = npzfile['terminal_values']
            return game
        except:
            return None

    def save(self, npz_file):
        np.savez_compressed(npz_file, states=self.states, history=self.history, child_visits=self.child_visits, terminal_values=[self.terminated, self.result, self.score, self.whiteScore])
    
    @staticmethod
    def loadpgn(pgn_file: str, startIndex: int = 0, num_games: int = -1):
        try:
            pgn = open(pgn_file, encoding="utf-8")
            i = 0
            while i < startIndex and chess.pgn.skip_game(pgn):
                i += 1
            i = 0                
            pgn_game = chess.pgn.read_game(pgn)
            while not pgn_game is None and (num_games == -1 or i < num_games):
                game = ChessGame(pgn_game.board(), pgn_game.mainline_moves(), pgn_game.headers['Result'])
                yield game
                pgn_game = chess.pgn.read_game(pgn)
                i += 1
        finally:
            pgn.close()

    def savepgn(self, pgn_file: str):
        try:
            game = chess.pgn.Game.from_board(self.board)
            now = datetime.datetime.now()
            game.headers.update({
                "Date": now.strftime("%Y-%m-%d"),
                "Result": self.getGameResult()
            })
            pgnString = game.accept(self.exporter) # export game pgn
            pgnfile = open(pgn_file, "w")
            pgnfile.write(pgnString)
        finally:
            pgnfile.close()

    def saveh5py(self, h5py_group):
        h5py_group.create_dataset("states", data=self.states, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("history", data=self.history, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("legals", data=self.legals, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("child_visits", data=self.child_visits, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("terminated", data=self.terminated)
        h5py_group.create_dataset("result", data=self.result)
        h5py_group.create_dataset("score", data=self.score)
        h5py_group.create_dataset("whiteScore", data=self.whiteScore)

    def __init__(self, initboard: chess.Board = chess.Board(), mainline_moves: [chess.Move] = None, result: str = None):
        self.states = []
        self.legals = []
        self.board = initboard # this is the board we use to track the entire game progress for both player 1 and player 2
        self.boards = np.array([None] * 8) # 8 half move history to track 3-fold repetitions
        self.boards[0] = self.board
        self.terminated = False
        self.result = np.nan
        self.score = np.nan
        self.whiteScore = np.nan
        self.exporter = chess.pgn.StringExporter(headers=True, variations=True, comments=True)
        super().__init__(np.prod(self.getActionSize()), [])
        if not mainline_moves is None:
            for move in mainline_moves:
                self.apply(actions[move.uci()])
        if not result is None:
            self.terminated = True
            self.setGameResult(result)
    
    def boardToState(self):
        # Alpha Zero's Chess State consists of 119 planes of 8*8
        state = np.zeros((119, n, n))
        i = 0
        transpositions = collections.Counter()
        for board in self.boards:
            if board is None:
                i += 6 + 6 + 2 # leave everything as zero per AlphaZero paper Table S1, pp 26
                continue
            key = board.fen().split(' ')[0]
            if key in BoardToState:
                end_plane = i + 6 + 6
                transposition_key, state[i:end_plane] = BoardToState[key]
                i = end_plane
            else:
                transposition_key = board._transposition_key()
                start_plane = i
                # 6 planes of binary (0, 1) features of player 2 (black)'s pieces [p n b r q k]
                # 6 planes of binary (0, 1) features of player 1 (white)'s pieces [P N B R Q K]
                for color in range(2):
                    for piece in range(6):
                        state[i] = np.reshape(board.pieces(piece + 1, color=color).tolist(), (n, n))
                        i += 1
                BoardToState[key] = (transposition_key, state[start_plane:i])
            transpositions.update((transposition_key, ))
            # 2 planes for repetitions
            state[i] = np.array([1 if transpositions[transposition_key] >= 2 else 0])
            i += 1
            state[i] = np.array([1 if transpositions[transposition_key] >= 1 else 0])
            i += 1
        # ------------------------------ Sub total = (6 + 6 + 2) * 8 = 112 planes
        # 1 plane for color of player: 0 for player 1, 1 for player 2
        state[i] = np.array([0 if self.board.turn else 1])
        i += 1
        # 1 plane for total move count
        state[i] = np.array([self.board.fullmove_number])
        i += 1
        # 1 plane for player 1's king side castle right
        state[i] = np.array([self.board.has_kingside_castling_rights(color=0)])
        i += 1
        # 1 plane for player 1's queen side castle right
        state[i] = np.array([self.board.has_queenside_castling_rights(color=0)])
        i += 1
        # 1 plane for player 2's king side castle right
        state[i] = np.array([self.board.has_kingside_castling_rights(color=1)])
        i += 1
        # 1 plane for player 2's queen side castle right
        state[i] = np.array([self.board.has_queenside_castling_rights(color=1)])
        i += 1
        # 1 plane for non-progress count
        state[i] = np.array([self.board.halfmove_clock])
        i += 1
        # ============================== Total 14 * 8 + 7 = 119 planes
        assert(i == 119)
        return state

    def makeMove(self, uci_action):
        self.boards[7] = self.boards[6]
        self.boards[6] = self.boards[5]
        self.boards[5] = self.boards[4]
        self.boards[4] = self.boards[3]
        self.boards[3] = self.boards[2]
        self.boards[2] = self.boards[1]
        self.boards[1] = chess.Board(fen=self.board.fen()) # take a snapshot of current board
        self.board.push(chess.Move.from_uci(uci_action))
        
    def makeMoveState(self, uci_action):
        self.makeMove(uci_action)
        return self.boardToState() # new board state
    
    def getValidActions(self, state):
        legal_uci_actions = [move.uci() for move in self.board.legal_moves]
        legal_actions = []
        for uciAction in legal_uci_actions:
            legal_actions.append(actions[uciAction])
        return legal_actions

    def getValidActionsMask(self, state):
        validActionsMask = np.zeros(self.getActionSize(), dtype=bool)
        for action in self.getValidActions(state):
            validActionsMask[action] = True
        return validActionsMask # multi-hot matrix of valid action masks
    
    def findBestMove(self, state, actionProbs):
        validActionsMask = self.getValidActionsMask(state)
        actionProbs *= validActionsMask
        actionProbs /= np.sum(actionProbs) # Renormalize after masking
        (plane, x0, y0) = np.unravel_index(np.argmax(actionProbs), actionProbs.shape) # Find best valid move
        uci_move = uciActions[plane][y0][x0]
        if plane < 56 and y0 == 6 and state[6][y0][x0] == 1:  # White pawn promotion to queen (automatic)
            uci_move += 'q'
        elif plane < 56 and y0 == 1 and state[0][y0][x0] == 1: # Black pawn promotion to queen (automatic)
            uci_move += 'q'
        return uci_move

    def findBestMoveMCTS(self, state, actionProbs):
        # should already be legal moves
        (plane, x0, y0) = np.unravel_index(np.argmax(actionProbs), actionProbs.shape) # Find best valid move
        uci_move = uciActions[plane][y0][x0]
        if plane < 56 and y0 == 6 and state[6][y0][x0] == 1:  # White pawn promotion to queen (automatic)
            uci_move += 'q'
        elif plane < 56 and y0 == 1 and state[0][y0][x0] == 1: # Black pawn promotion to queen (automatic)
            uci_move += 'q'
        return uci_move

    def getValidActionAndMoveProb(self, state, actionProbs):
        validActionsMask = self.getValidActionsMask(state)
        actionProbs *= validActionsMask
        actionProbs /= np.sum(actionProbs) # Renormalize
        planes, x0s, y0s = np.nonzero(actionProbs)

        ActionAndMoveProb = []
        for idx in range(len(planes)):
            plane, y0, x0 = planes[idx], y0s[idx], x0s[idx]
            ActionAndMoveProb.append((uciActions[plane,y0,x0], actionProbs[plane,x0,y0])) # THIS IS FLIPPED!!
            
        return ActionAndMoveProb


    def getGameEnded(self):
        if not self.terminated:
            # Game specific termination rules.
            self.terminated = self.board.is_game_over(claim_draw=True)
            if self.terminated:
                self.setGameResult(self.board.result(claim_draw=True))
        return self.terminated

    def getGameResult(self):
        return self.result

    def setGameResult(self, result: str):
        self.result = result
        if self.result == '1-0':
            self.whiteScore = 1
            self.score = 1  # white win
        elif self.result == '0-1':
            self.whiteScore = 0
            self.score = -1  # white lose
        else:
            self.whiteScore = 0.5
            self.score = 0 # draw

    def getBoardSize(self):
        return (119, n, n)

    def getActionSize(self):
        return (73, n, n)

    def toString(self):
        return self.board.fen().split(' ') # current board's fen is a good key

    def getScore(self, to_play):
        # Called when board game is over, note to_play is always the losing side after checkmate
        return -self.score if self.to_play() == to_play else self.score
        
    def getWhiteScore(self):
        return self.whiteScore

class ChessTest:
    n = 8

    def __init__(self):
        pass

    def generateUciChessActions(self):
        squares = [(i, j) for i in range(n) for j in range(n)]
        actions = []
        for pickUp in squares:
            for dropOff in squares:
                if pickUp == dropOff:
                    continue
                h_diff = abs(dropOff[1] - pickUp[1])
                v_diff = abs(dropOff[0] - pickUp[0])
                if (h_diff == 0 or v_diff == 0 or h_diff == v_diff): # horizontal, vertical or diagonal moves
                    pass
                else:
                    if (h_diff == 1 and v_diff == 2) or (h_diff == 2 and v_diff == 1): # knight's moves
                        pass
                    else:
                        continue
                move = chr(ord('a') + pickUp[0]) + str(pickUp[1] + 1) + chr(ord('a') + dropOff[0]) + str(dropOff[1] + 1)
                actions.append(move)
                if dropOff[1] == n - 1 and dropOff[1] - pickUp[1] == 1 and abs(pickUp[0] - dropOff[0]) <= 1:
                    for promotion in ['q', 'r', 'b', 'n']:
                        actions.append(move + promotion) # white promotions
                elif dropOff[1] == 0 and pickUp[1] - dropOff[1] == 1 and abs(pickUp[0] - dropOff[0]) <= 1:
                    for promotion in ['q', 'r', 'b', 'n']:
                        actions.append(move + promotion) # black promotions
        return np.array(actions)

    def generateInvalidUciChessActions(self):
        squares = [(i, j) for i in range(n) for j in range(n)]
        actions = []
        for pickUp in squares:
            for dropOff in squares:
                if pickUp == dropOff:
                    move = chr(ord('a') + pickUp[0]) + str(pickUp[1] + 1) + chr(ord('a') + dropOff[0]) + str(dropOff[1] + 1)
                    actions.append(move)
        return np.array(actions)
                
    def testActionsDictionary(self, actions):
        for action in self.generateUciChessActions():
            assert action in actions, "Missing action {0}".format(action)
        for invalidAction in self.generateInvalidUciChessActions():
            assert not invalidAction in actions, "Illegal action {0} included".format(invalidAction)
        print("testActionsDictionary : passed")
    
    def testUciActions(self, uciActions):
        assert uciActions.shape == (73, 8, 8), "Invalid chess action space {0}".format(uciActions.shape)
        validUciActions = set(self.generateUciChessActions())
        for plane in range(uciActions.shape[0]):
            for y in range(uciActions.shape[1]):
                for x in range(uciActions.shape[2]):
                    if not uciActions[plane][y][x] is None:
                        assert uciActions[plane][y][x] in validUciActions, "Invalid action ({0},{1},{2}) = {3} found".format(plane, y, x, uciActions[plane][y][x])
        print("testUciActions : passed")
    
    def testBoardToState(self, state, boards):
        assert boards.shape == (8, ), "Invalid chess boards history {0}".format(boards.shape)
        assert state.shape == (119, 8, 8), "Invalid chess state space {0}".format(state.shape)
        for i in range(len(boards)):
            plane = i * (6 + 6 + 2)
            if boards[i] is None:
                for j in range(6):
                    assert (state[plane + j] == 0).all(), "Non-zero state for empty board {0}".format(i)
            else:
                assert (state[plane][6] == np.ones(8)).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane, 6, state[plane][6])
                assert (state[plane + 1][7] == np.array([0, 1, 0, 0, 0, 0, 1, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 knights".format(plane + 1, 7, state[plane + 1][7])
                assert (state[plane + 2][7] == np.array([0, 0, 1, 0, 0, 1, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 bishops".format(plane + 2, 7, state[plane + 2][7])
                assert (state[plane + 3][7] == np.array([1, 0, 0, 0, 0, 0, 0, 1])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 rooks".format(plane + 3, 7, state[plane + 3][7])
                assert (state[plane + 4][7] == np.array([0, 0, 0, 1, 0, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with queen".format(plane + 4, 7, state[plane + 4][7])
                assert (state[plane + 5][7] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with king".format(plane + 5, 7, state[plane + 5][7])
                assert (state[plane + 6][1] == np.ones(8)).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane + 6, 1, state[plane + 6][1])
                assert (state[plane + 7][0] == np.array([0, 1, 0, 0, 0, 0, 1, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 knights".format(plane + 7, 0, state[plane + 7][0])
                assert (state[plane + 8][0] == np.array([0, 0, 1, 0, 0, 1, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 bishops".format(plane + 8, 0, state[plane + 8][0])
                assert (state[plane + 9][0] == np.array([1, 0, 0, 0, 0, 0, 0, 1])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 rooks".format(plane + 9, 0, state[plane + 9][0])
                assert (state[plane + 10][0] == np.array([0, 0, 0, 1, 0, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with queen".format(plane + 10, 0, state[plane + 10][0])
                assert (state[plane + 11][0] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with king".format(plane + 11, 0, state[plane + 11][0])
        assert state[112][0][0] == 0, "state[{0}][{1}][{2}] = {3} was not initialized correctly with player color {4}".format(112, 0, 0, state[112][0][0], 0)
        print("testBoardToState : passed")
    
    def testMakeMove(self, state, boards):
        assert boards.shape  == (8, ), "Invalid chess boards history {0}".format(boards.shape)
        assert state.shape == (119, 8, 8), "Invalid chess state space {0}".format(state.shape)
        for i in range(len(boards)):
            plane = i * (6 + 6 + 2)
            if boards[i] is None:
                for j in range(6):
                    assert (state[plane + j] == 0).all(), "Non-zero state for empty board {0}".format(i)
            elif i == 1:
                assert (state[plane][6] == np.ones(8)).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane, 6, state[plane][6])
                assert (state[plane + 1][7] == np.array([0, 1, 0, 0, 0, 0, 1, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 knights".format(plane + 1, 7, state[plane + 1][7])
                assert (state[plane + 2][7] == np.array([0, 0, 1, 0, 0, 1, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 bishops".format(plane + 2, 7, state[plane + 2][7])
                assert (state[plane + 3][7] == np.array([1, 0, 0, 0, 0, 0, 0, 1])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 rooks".format(plane + 3, 7, state[plane + 3][7])
                assert (state[plane + 4][7] == np.array([0, 0, 0, 1, 0, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with queen".format(plane + 4, 7, state[plane + 4][7])
                assert (state[plane + 5][7] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with king".format(plane + 5, 7, state[plane + 5][7])
                assert (state[plane + 6][1] == np.ones(8)).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane + 6, 1, state[plane + 6][1])
                assert (state[plane + 7][0] == np.array([0, 1, 0, 0, 0, 0, 1, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 knights".format(plane + 7, 0, state[plane + 7][0])
                assert (state[plane + 8][0] == np.array([0, 0, 1, 0, 0, 1, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 bishops".format(plane + 8, 0, state[plane + 8][0])
                assert (state[plane + 9][0] == np.array([1, 0, 0, 0, 0, 0, 0, 1])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 rooks".format(plane + 9, 0, state[plane + 9][0])
                assert (state[plane + 10][0] == np.array([0, 0, 0, 1, 0, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with queen".format(plane + 10, 0, state[plane + 10][0])
                assert (state[plane + 11][0] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with king".format(plane + 11, 0, state[plane + 11][0])
            elif i == 0:
                assert (state[plane][6] == np.ones(8)).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane, 6, state[plane][6])
                assert (state[plane + 1][7] == np.array([0, 1, 0, 0, 0, 0, 1, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 knights".format(plane + 1, 7, state[plane + 1][7])
                assert (state[plane + 2][7] == np.array([0, 0, 1, 0, 0, 1, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 bishops".format(plane + 2, 7, state[plane + 2][7])
                assert (state[plane + 3][7] == np.array([1, 0, 0, 0, 0, 0, 0, 1])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 rooks".format(plane + 3, 7, state[plane + 3][7])
                assert (state[plane + 4][7] == np.array([0, 0, 0, 1, 0, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with queen".format(plane + 4, 7, state[plane + 4][7])
                assert (state[plane + 5][7] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with king".format(plane + 5, 7, state[plane + 5][7])
                assert (state[plane + 6][1] == np.array([1, 1, 1, 1, 0, 1, 1, 1])).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane + 6, 1, state[plane + 6][1])
                assert (state[plane + 6][3] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized with pawns".format(plane + 6, 1, state[plane + 6][1])
                assert (state[plane + 7][0] == np.array([0, 1, 0, 0, 0, 0, 1, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 knights".format(plane + 7, 0, state[plane + 7][0])
                assert (state[plane + 8][0] == np.array([0, 0, 1, 0, 0, 1, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 bishops".format(plane + 8, 0, state[plane + 8][0])
                assert (state[plane + 9][0] == np.array([1, 0, 0, 0, 0, 0, 0, 1])).all(), "state[{0}][{1}] = {2} was not initialized correctly with 2 rooks".format(plane + 9, 0, state[plane + 9][0])
                assert (state[plane + 10][0] == np.array([0, 0, 0, 1, 0, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with queen".format(plane + 10, 0, state[plane + 10][0])
                assert (state[plane + 11][0] == np.array([0, 0, 0, 0, 1, 0, 0, 0])).all(), "state[{0}][{1}] = {2} was not initialized correctly with king".format(plane + 11, 0, state[plane + 11][0])
        assert state[112][0][0] == 1, "state[{0}][{1}][{2}] = {3} was not initialized correctly with player color {4}".format(112, 0, 0, state[112][0][0], 1)
        print("testMakeMove : passed")
    
    def testFindBestMove(self, chessGame):
        np.random.seed(1)
        state = chessGame.boardToState()
        shape = uciActions.shape
        actionProbs = softmax(np.random.randn(shape[0], shape[1], shape[2]))
        uci_best_move = chessGame.findBestMove(state, actionProbs)
        legal_actions = { move.uci() for move in chessGame.board.legal_moves }
        assert uci_best_move in legal_actions, "{0} is not a legal move in {1}".format(uci_best_move, legal_actions) 
        print("testFindBestMove : passed")

    def testGameEnded(self, chessGame):
        result = chessGame.getGameEnded()
        assert result == 0, "Game should not end yet but result = {0}".format(result)
        chessGame.makeMove("g8f6")
        chessGame.makeMove("g1f3")
        chessGame.makeMove("f6g8")
        chessGame.makeMove("f3g1")
        chessGame.makeMove("g8f6")
        chessGame.makeMove("g1f3")
        chessGame.makeMove("f6g8")
        chessGame.makeMove("f3g1")
        chessGame.makeMove("g8f6")
        chessGame.makeMove("g1f3")
        chessGame.makeMove("f6g8")
        chessGame.makeMove("f3g1")
        result = chessGame.getGameEnded()
        assert result == True, "Game should be drawn but result = {0}".format(result)
        print("testGameEnded : passed")
    
    def testBlackPawnPromotion(self):
        chessGame = ChessGame(chess.Board())
        result = chessGame.getGameEnded()
        assert result == False, "Game should not end yet but result = {0}".format(result)
        for move in "e2e4 d7d5 e4d5 g7g6 d5d6 b8c6 d6c7 f8h6 c7d8q c6d8 h2h3 a8b8 h3h4 e8d7 h4h5 f7f6 g2g3 b8a8 g3g4 e7e6 f2f3 d7c6 c2c3 h6g7 d2d4 c6d6 g1e2 f6f5 f1g2 g8h6 d1c2 g7e5 c2e4 h6f7 b1a3 e5f4 c1f4 f7e5 f4e5 d6d7 e5c7 b7b5 c7d8 a7a6 d8f6 f5g4 f6h8 a8a7 h8e5 d7e7 e5b8 a6a5 b8a7 c8b7 e4b7 e7d8 a7b6 d8e8 b7c7 h7h6 b6c5 g6g5 e2f4 a5a4 b2b3 a4b3 a1b1 e6e5 g2f1 g4g3 h1h2 e5d4 f1e2 b5b4 e2d3 g3h2 f4g2".split(' '):
            chessGame.makeMove(move)
        actionProbs = np.zeros(chessGame.getActionSize())
        actionProbs[28][7][1] = 1 # h2h1q
        state = chessGame.boardToState()
        action = chessGame.findBestMove(state, actionProbs)
        assert action=="h2h1q", "Black pawn was not correctly promoted to queen action = {0}".format(action)
        print("testBlackPawnPromotion : passed")

if __name__=="__main__":
    chessGame = ChessGame(chess.Board())
    chessGameTest = ChessTest()
    chessGameTest.testActionsDictionary(actions)
    chessGameTest.testUciActions(uciActions)
    chessGameTest.testBoardToState(chessGame.boardToState(), chessGame.boards)
    chessGameTest.testMakeMove(chessGame.makeMoveState("e2e4"), chessGame.boards)
    chessGameTest.testFindBestMove(chessGame)
    chessGameTest.testGameEnded(chessGame)
    chessGameTest.testBlackPawnPromotion()
