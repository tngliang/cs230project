#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""
from __future__ import print_function

from AlphaZero import run_mcts
from AlphaZeroConfig import AlphaZeroConfig
from ChessEngine import ChessEngine
from ChessGame import ChessGame
from ChessGame import uciActions
from ChessNetwork import ChessNetwork

class NeuralChessZeroChessEngine(ChessEngine):
    def __init__(self, config: AlphaZeroConfig, network: ChessNetwork, step: int):
        self.name = "NeuralChessZero {0}".format(step)
        self.config = config
        self.config.num_sampling_moves = 0 # do not sample during evaluation
        self.network = network
        super().__init__(self.network.elo) # default for chess

    def play(self, game: ChessGame):
        if not game.getGameEnded():
            action, root = run_mcts(self.config, game, self.network)
            game.apply(action)
            game.store_search_statistics(root)
            plane, x0, y0 = action
            #print("{0} played {1}".format(self.name, uciActions[plane][y0][x0]))
            return uciActions[plane][y0][x0]


