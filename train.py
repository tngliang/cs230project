# AlphaZero notes:
# 700,000 steps (minibatch = 4096 training positions)
# 5000 TPIs used during training (self play).
# Trained for 9 hours in chess
# Outperformed Stockfish  in 4 hours (300,000 steps). Result was repeatable
# Alphazero beat stockfish (155 win, 6 loss, 1000 games)
# search 60,000 positions/second
# games exceeding 512 steps were terminated and assigned a draw

# During training:
# each MCTS = 800 simulations
# learning rate = 0.2 (0.02 @ 100,000 steps, 0.002 @ 300,000 steps, 0.0002 @ 500,000steps)
# 44 mil games

# During eval:
# AlphaZero selects moves greedily w.r.t. root visit count
# Each MCTS was executed on single machine with 4 1st-gen TPUs.
# vs stockfish version 8 as baseline
# mostly draws (90%+). 
# Softmax sampling with temperature of 10.0 among moves for which values were less than 1% away from best move for 1st 30 plies helped inrease winning rate from 5.8% to 14%
# 3 types of matches were performed: default board state, human opening positions, 2016 TCEC opening positions

# from osbrain import run_agent
# from osbrain import run_nameserver
# from osbrain import Agent
import time
import util 
from LiaCha_model import LiaCha
from LiaCha_simple import LiaCha_simple
from StockfishChessEngine import StockfishChessEngine
from LiaCha_Engine import LiaChaEngine

import argparse
from AlphaZeroConfig import AlphaZeroConfig
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.optim.lr_scheduler as sched
import torch.utils.data as data
from torch.autograd import Variable
import os
import time
from tqdm import tqdm
import pdb
from tensorboardX import SummaryWriter
from ChessGame import actions as actionsMAP
from MCTS import MCTS
from ChessTournament import ChessTournament

def lossfn(output_actions, predictions, goldmoves, results, tbx = None, step = None):
    batch_size = output_actions.size()[0]
    
    # mean squared error between prediction and actual result
    z_v = nn.MSELoss(reduction='mean')(input = predictions, target = results)
    
    # cross entropy loss between prediction and actual result
    output_actions = output_actions.view(batch_size,-1)

    logp = torch.log(output_actions)
    piTlogp = torch.mm(torch.t(goldmoves),logp)
    piTlogp = - torch.sum(piTlogp)/batch_size
    # piTlogp = nn.CrossEntropyLoss()(input = output_actions, target = goldmoves) #target should always have dtype = long
    if tbx is not None:
        tbx.add_scalar('train/loss (pred)', z_v.item(), step)
        tbx.add_scalar('train/loss (moveprob)', piTlogp.item(), step)
    return z_v + piTlogp #cross entropy is already minus in pytorch

def main(args):
    # get devices
    device, gpu_ids = util.get_available_devices()
    print(device)
    print(gpu_ids)

    # tensorboard
    tbx = SummaryWriter(util.get_save_dir(args.output_dir + '/tboard'))

    # the enemy
    stockfish = StockfishChessEngine()

    if args.train_file:
        print("loading train_file from :" + str(args.train_file))
        moves, states, results = torch.load(f = args.train_file)

    else:
        # import data
        if args.pgn_file:
            if not os.path.exists(args.pgn_file):
                raise Exception('pgn file does not exist!')

            print("loading pgn_file from :" + str(args.pgn_file))
            pgn_file = args.pgn_file
        else:
            print("training default pgn_file")
            pgn_file = '.\data\ChessTournament01.pgn'
        moves, states, results = util.supervised_pgn_read(pgn_file, num_games = 1000) 
        torch.save((moves, states, results), './data/train.pt')       
    
    # import model
    model = LiaCha_simple()
    if args.model:
        print("loading model from :" + str(args.model))
        model.load_state_dict(torch.load(os.path.join(args.model)))
    else:
        print("training brand new model")
        
    model.to(device)
    model.train()

    # Get optimizer and scheduler
    config = AlphaZeroConfig()
    optimizer = optim.Adam(
                        params = model.parameters(), 
                        lr = 0.01 #config.learning_rate_schedule[0], 
                        # betas = (config.momentum, 0.99) #(config.momentum, 0.99)
                        # weight_decay=config.weight_decay #L2 regularization
                        ) 
    scheduler = sched.MultiStepLR(optimizer, milestones=[int(1e7),int(3e7),int(5e7)], gamma=0.1) #currently hardcoded...
    # scheduler = sched.MultiStepLR(optimizer, milestones=[100e3,300e3,500e3], gamma=0.1) #currently hardcoded...
    torch.enable_grad()

    totalexamples = moves.size()[0]
    print("Total number of examples: " + str(totalexamples))
    batch_size = args.batch_size
    if batch_size > totalexamples:
        batch_size = totalexamples
    
    params = {'batch_size': batch_size,
          'shuffle': True}
    training_set = util.Dataset(moves, states, results)
    training_generator = data.DataLoader(training_set, **params)

    
    step = 0
    examplecount = 0
    steps_till_eval = 0

    for epoch in range(args.epochs):
        print('Training...')
        print('Starting epoch {}...'.format(epoch))

        for moves_mini, states_mini, results_mini in tqdm(training_generator):
            moves_mini, states_mini, results_mini = moves_mini.to(device, dtype=torch.float), states_mini.to(device, dtype=torch.float), results_mini.to(device, dtype=torch.float)
            # zero gradients between batches
            optimizer.zero_grad()

            # Forward
            output_actions, predictions = model(states_mini)
            output_actions, predictions = output_actions.to(device), predictions.to(device)
            
            loss  = lossfn(output_actions, predictions, moves_mini, results_mini, tbx, examplecount + batch_size)

            # Backward
            loss.backward() # accumulate gradient
            nn.utils.clip_grad_norm_(model.parameters(), 5)
            optimizer.step() # Optimizer step is performed on every mini batch, while 
            scheduler.step(examplecount // batch_size) #scheduler step is usually performed per-epoch.
            
            step += 1
            examplecount += batch_size 
            tbx.add_scalar('train/loss', loss.item(), examplecount)
            steps_till_eval += 1 
            
            # # run tournament: compute and upload elo score
            # if steps_till_eval > config.checkpoint_interval:
            #     model.eval()
            #     modelEngine = LiaChaEngine(AlphaZeroConfig(), model, thinkingTime = 800)
            #     model.to('cpu')
            #     tournament = ChessTournament(num_games = 1)
            #     tournament.Run(step, player1 = stockfish, player2 = modelEngine, config = AlphaZeroConfig())

            #     print("current elo: " + str(model.elo))
            #     tbx.add_scalar('train/elo', model.elo, examplecount)
            #     model.to(device)
            #     model.train()
            #     steps_till_eval = 0


    # save to output_dir
    timestr = time.strftime("%Y%m%d-%H%M%S")
    torch.save(model.state_dict(), args.output_dir + timestr + '.pt')


"""
examples:
python train.py --output_dir ./LiaCha_models --train_file ./data/train.pt --batch_size 1024 --epochs 30

make sure you're in the top dir
tensorboard --logdir ./LiaCha_models/tboard --port 5678
http://localhost:5678/
"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--pgn_file", default=None, type=str, required=False,help="")
    parser.add_argument("--model", default=None, type=str, required=False,help="")
    parser.add_argument("--train_file", default=None, type=str, required=False,help="")
    parser.add_argument("--output_dir", default=None, type=str, required=True,help="")
    parser.add_argument("--batch_size", default=4096, type=int, required=False,help="")
    parser.add_argument("--epochs", default=10, type=int, required=False,help="")
    # parser.add_argument("--model", action='store_true', help="Whether to run training.")
    # parser.add_argument("--do_predict", action='store_true', help="Whether to run eval on the dev set.")
    
    args = parser.parse_args()
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

        if not os.path.exists(args.output_dir + '/tboard'):
            os.makedirs(args.output_dir + '/tboard')

    if not os.path.exists(args.train_file):
        raise Exception('train file does not exist!')
    
    
    main(args)

"""
# TODO:
1. add # of games to load
3. hyperparameter search
4. DataLoader w shuffle
5. milestone learning rate schedule currently hardcoded
6. save load from dict

"""