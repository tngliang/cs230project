#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""
from __future__ import print_function
import chess
import chess.engine

import pdb

from ChessGame import ChessGame
from ChessGame import actions
from ChessEngine import ChessEngine

class StockfishChessEngine(ChessEngine):
    def __init__(self):
        self.name = "Stockfish_10_x64"
        self.engine = chess.engine.SimpleEngine.popen_uci(self.name)
        super().__init__(3232) # based on http://legacy-tcec.chessdom.com/archive.php
  
    def play(self, game: ChessGame):
        if not game.getGameEnded():
            retry = True
            while retry:
                try:
                    board = chess.Board()
                    for move in game.board.move_stack:
                        board.push(move)
                    result = self.engine.play(board, chess.engine.Limit(time=None))
                    uciMove = result.move.uci()
                    game.apply(actions[uciMove])
                    #print("Stockfish played {0}".format(uciMove))
                    retry = False
                    return uciMove
                except:
                    pdb.set_trace()
                    quit()
                    self.engine = chess.engine.SimpleEngine.popen_uci(self.name)

    def quit(self):
        self.engine.quit()

class StockfishChessEngineTest:
    def testStockfishEngineCrash(self):
        game = ChessGame(chess.Board())
        for move in "g2g3 c7c5 b1c3 d7d5 e2e3 d5d4 f1g2 d4c3 e3e4 b8c6 g1f3 c3d2 e1f1 d2c1q f3g5".split(' '):
            game.makeMove(move)
        engine = StockfishChessEngine()
        engine.play(game)
        engine.quit()
        print("testStockfishEngineCrash : passed")

if __name__=="__main__":
    test = StockfishChessEngineTest()
    test.testStockfishEngineCrash()