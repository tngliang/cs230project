from ChessGame import ChessGame, uciActions, actions
import pdb
import chess
import torch 

from torch.utils.data import DataLoader
import util
import pickle


from MCTS import MCTS
from LiaCha_model import LiaCha
from LiaCha_simple import LiaCha_simple
from StockfishChessEngine import StockfishChessEngine
from AlphaZeroConfig import AlphaZeroConfig
from ChessTournament import ChessTournament
from LiaCha_Engine import LiaChaEngine
# ex = moves, states, results
# ex = torch.load(f = 'train.pt')
# with open(r"train.pkl", "wb") as output_file:
#     pickle.dump(ex, output_file)




import random
import numpy as np


engine = chess.engine.SimpleEngine.popen_uci("Stockfish_10_x64")

simulations = 20
games = 10

moves = []
states = []
results = []

examples = 0

for game in range(games):
    chessobj = ChessGame(chess.Board(chess960=False))
    board_main = chess.Board(chess960=False)
    movecount = 0
    # while not board_main.is_game_over():
    while not chessobj.board.is_game_over():

        state = chessobj.boardToState()
        state = torch.tensor(state, dtype = torch.float)
        states.append(state)
        
        moveProb = torch.zeros([73, 8, 8], dtype = torch.float)
        
        # note: seems like we don't actually need to create sims
        # board_sim = chess.Board(fen = board_main.fen())
        board_sim = chess.Board(fen = chessobj.board.fen())
        for n in range(simulations):
            # result = engine.play(chessobj.board, chess.engine.Limit(time=None))
            print(board_sim)
            result = engine.play(board_sim, chess.engine.Limit(time=None))
            plane, x0, y0 = actions[result.move.uci()]
            moveProb[plane, x0, y0] += 1

            # print(result.move)
            # print(uciActions[plane, y0, x0])
            # board_sim.push.from_uci(result.move)        
            board_sim = chess.Board(fen = chessobj.board.fen())
            
        actionProbs = moveProb.cpu().numpy()
        (plane, x0, y0) = np.unravel_index(np.argmax(actionProbs), actionProbs.shape)
        # print(chessobj.board)
        # print(uciActions[plane, y0, x0])
        # pdb.set_trace()
        chessobj.board.push(chess.Move.from_uci(uciActions[plane, y0, x0]))
        movecount += 1

        moveProb /= torch.sum(moveProb)
        moveProb = moveProb.view(-1)
        moves.append(moveProb)

        if movecount > 512:
            break
    
    result = util.interpretResult(chessobj.board.result(claim_draw=True)) # TODO: delete util part
    result = torch.tensor([result], dtype = torch.float)
    for _ in range(movecount):
        results.append(result)
    
    examples +=movecount
    print("game: " + str(game))
    print("movecount: " + str(movecount))
    print("examples: " + str(examples))


moves, states, results = torch.stack(moves), torch.stack(states), torch.stack(results)

torch.save((moves, states, results), f = './data/train.pt')
# torch.save((moves, states, results), f = './data/train-{:02d}.pt'.format(set)) 












































# while not pgn_game is None:
#     game = ChessGame(pgn_game.board(), pgn_game.mainline_moves(), pgn_headers['Result'])
#     yield game
#     pgn_game = chess.pgn.read_game(pgn)
#     pgn_headers = chess.pgn.read_headers(pgn)



# # class gameAgent(Agent):
# #   def on_init(self):
# #     self.bind('PUSH', alias = 'gameChannel')

# #   def start(self):
# #     start = time.time()
# #     while True:
# #       time.sleep(1)
# #       self.sendGame(1)
# #       if time.time() - start > 10: 
# #         break
# #     print(agent.name + " complete!")
# #   def sendGame(self, game):
# #     self.send('gameChannel', game)

# # def game(agent, buffernetwork):
# #   buffer, network = buffernetwork
# #   start = time.time()
# #   print(agent.name + " starting!")
# #   while True:
# #     time.sleep(1)
# #     buffer.append(1)
# #     print(buffer)
# #     if time.time() - start > 10: 
# #       break

# #   return agent.name + " complete!"

# # if __name__ == '__main__':
# #   agents = {}
# #   ns = run_nameserver()
# #   # addr = alice.bind('REP', alias='main', handler=reply)
# #   # bob
# #   buffer = []
# #   network = {}
# #   start = time.time()

# #   agents['mainKingKong'] = run_agent('mainKingKong')
# #   addr = agents['mainKingKong'].bind('PULL')

# #   for i in range(3):
# #     agent_name = 'agent' + str(i)
# #     print(agent_name + ' created!')
# #     agents[agent_name] = run_agent(agent_name, base = gameAgent)
# #     agents[agent_name].connect(addr, handler = game)
  
# #   agents['mainKingKong'].send('begin', (buffer, network))

# #   while True:
# #     time.sleep(2)
# #     print(buffer)
# #     if time.time() - start > 20:
# #       break
# #   ns.shutdown()
# #########################################################
# from multiprocessing import Process, Manager

# def game(buffer, network):
#   start = time.time()
#   count = 0
#   print("started!")
#   while True:
#     buffer.append(1)
#     network[count] = 0
#     count += 1
#     time.sleep(1)
#     print("*")
#     if time.time() - start > 3:
#       break
  
# if __name__ == '__main__':
#   start = time.time()
#   with Manager() as manager:
#     buffer = manager.list()
#     buffer.append(1)
#     network = manager.dict()
#     network[111111] = 0
#     agents = {}
#     for i in range(3):
#       agent_name = 'agent' + str(i)
#       print(agent_name + ' created!')
#       agents[agent_name] = Process(target = game, args = (buffer, network))
#       agents[agent_name].daemon = True
#       agents[agent_name].start()
    
#     while True:
#       time.sleep(1)
#       print(buffer)
#       print(network)
#       if time.time() - start > 20:
#         break

# #########################
# # from multiprocessing import Process, Manager

# # def f(d, l):
# #     d[1] = '1'
# #     d['2'] = 2
# #     d[0.25] = None
# #     l.reverse()

# # if __name__ == '__main__':
# #     with Manager() as manager:
# #         d = manager.dict()
# #         l = manager.list(range(10))

# #         print(d)
# #         print(l)
# #         p = Process(target=f, args=(d, l))
# #         p.start()
# #         p.join()

# #         print(d)
# #         print(l)