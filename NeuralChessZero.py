import argparse

import chess
import chess.pgn
import numpy as np

from AlphaZeroConfig import AlphaZeroConfig
from AlphaZero import find_model
from ChessGame import ChessGame
from ChessNetwork import ChessNetwork
from NeuralChessZeroEngine import NeuralChessZeroChessEngine
from StockfishChessEngine import StockfishChessEngine

import pdb

def main(args):
    config = AlphaZeroConfig()
    if args.player is None or args.player == 'NeuralChessZero':
        if args.checkpoint is None:
            step = 0
            step, _ = find_model(config, step)
        else:
            step = (int(args.checkpoint))
        player = NeuralChessZeroChessEngine(config, ChessNetwork(config, "checkpoint", step), step)
    else:
        player = StockfishChessEngine()

    # print name of chess engine
    print('Starting server: NeuralChessZero.exe')
    exporter = chess.pgn.StringExporter(headers=True, variations=True, comments=True)
    chessGame = ChessGame(chess.Board())
    while chessGame.getGameEnded() == 0:
        cmd = input()
        if cmd == 'quit':
            pgnfile = open("NeuralChessZeroGames.pgn", "w")
            game = chess.pgn.Game.from_board(chessGame.board)
            pgnfile.write(game.accept(exporter))
            pgnfile.write("\n\n")
            pgnfile.close()
            break
        elif cmd == 'uci':
            print('NeuralChessZero by Ting Liang, Mark Chang')
            print('id name NeuralChessZero')
            print('id author Ting Liang, Mark Chang')
            print()
            print('option name Debug Log File type string default')
            print('option name Contempt type spin default 24 min -100 max 100')
            print('option name Analysis Contempt type combo default Both var Off var White var Black var Both')
            print('option name Threads type spin default 1 min 1 max 512')
            print('option name Hash type spin default 16 min 1 max 131072')
            print('option name Clear Hash type button')
            print('option name Ponder type check default false')
            print('option name MultiPV type spin default 1 min 1 max 500')
            print('option name Skill Level type spin default 20 min 0 max 20')
            print('option name Move Overhead type spin default 30 min 0 max 5000')
            print('option name Minimum Thinking Time type spin default 20 min 0 max 5000')
            print('option name Slow Mover type spin default 84 min 10 max 1000')
            print('option name nodestime type spin default 0 min 0 max 10000')
            print('option name UCI_Chess960 type check default false')
            print('option name UCI_AnalyseMode type check default false')
            print('option name SyzygyPath type string default <empty>')
            print('option name SyzygyProbeDepth type spin default 1 min 1 max 100')
            print('option name Syzygy50MoveRule type check default true')
            print('option name SyzygyProbeLimit type spin default 7 min 0 max 7')
            print('uciok')
        elif cmd == 'setoption':
            pass
        elif cmd == 'isready':
            print('readyok')
        elif cmd == 'ucinewgame':
            pass
        elif cmd.startswith('position'):
            params = cmd.split(' ')
            expectMoves = False
            for i in range(len(params)):
                if params[i] == 'fen':
                    i += 1
                    chessGame = ChessGame(chess.Board(fen=params[i]))
                elif params[i] == 'startpos':
                    chessGame = ChessGame(chess.Board())
                elif params[i] == 'moves':
                    expectMoves = True
                    i += 1
                elif expectMoves:
                    chessGame.makeMove(params[i])
        elif cmd.startswith('go'):
            params = cmd.split(' ')
            ponder = False
            findMove = False
            for i in range(len(params)):
                if params[i] == 'ponder':
                    ponder = True
                    break
                elif not ponder:
                    findMove = True
            if findMove and chessGame.board.is_valid():
                uciAction = player.play(chessGame)
                print("bestmove {0}".format(uciAction))
        elif cmd == 'stop':
            ponder = False
        else:
            pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-cpu', '--cpu', action='store_true')
    parser.add_argument('-p', '--player', help='engine name')
    parser.add_argument('-c', '--checkpoint', help='checkpoint for engine')
    args = parser.parse_args()
    main(args)
