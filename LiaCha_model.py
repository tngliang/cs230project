#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""

import sys
import torch
import torch.nn as nn
# import torch.nn.functional as F

class BasicBlock(nn.Module):
    # AlphaZero: 1 Body conv block = 2 rectified (ReLU), batch-norm conv layer w/ skip connections
    # AlphaZero: filter size = 256, k = 3 x 3, s = 1

    def __init__(self, in_planes = 256, planes = 256, stride = 1, kernel_size = 3, padding = 1, bias = False):
        super(BasicBlock, self).__init__()
        self.expansion = 1
        self.in_planes = in_planes
        self.planes = planes
        self.stride = stride
        self.kernel_size = kernel_size
        self.padding = padding
        self.bias = bias

        self.conv1 = nn.Conv2d(in_channels = in_planes, out_channels = planes, stride = stride, kernel_size = kernel_size, padding = padding, bias = bias)
        self.batchNorm1 = nn.BatchNorm2d(num_features = planes)
        self.conv2 = nn.Conv2d(in_channels = planes, out_channels = planes, stride = stride, kernel_size = kernel_size, padding = padding, bias = bias)
        self.batchNorm2 = nn.BatchNorm2d(num_features = planes)
        self.shortcut = nn.Sequential()

        if stride != 1 or in_planes != self.expansion * planes: #if the input and output shapes don't match
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion * planes, stride = 1, kernel_size = 1, bias = False),
                nn.BatchNorm2d(self.expansion * planes) # resize using 1x1 convolution to desired number of channels
            )

    def forward(self, x):
        output = self.conv1(x)
        output = self.batchNorm1(output)
        output = nn.ReLU()(output)
        output = self.conv2(output)
        output = self.batchNorm2(output)
        output = nn.ReLU()(output + self.shortcut(x))
        return output

class nnBody(nn.Module):
    # AlphaZero: 1 Rect Batch-norm conv layer -> resnetwork (19 x resblocks)
    # AlphaZero: input = 119x8x8 state space

    def __init__(self, block = BasicBlock, num_blocks = 19, in_planes = 119):
        super(nnBody, self).__init__()
        inputBlock = block()

        self.in_planes = in_planes 
        self.planes = inputBlock.planes
        self.stride = inputBlock.stride, 
        self.kernel_size = inputBlock.kernel_size, 
        self.padding = inputBlock.padding, 
        self.bias = inputBlock.bias

        self.conv1 = nn.Conv2d(in_channels = in_planes, out_channels = inputBlock.planes, stride = inputBlock.stride, kernel_size = inputBlock.kernel_size, padding = inputBlock.padding, bias = inputBlock.bias)
        self.batchNorm1 = nn.BatchNorm2d(num_features = inputBlock.planes)
        self.resNetwork = self.makeLayer(block = block, num_blocks = num_blocks)
    
    def makeLayer(self, block, num_blocks):
        layers = [block() for n in range(num_blocks)]
        return nn.Sequential(*layers)

    def forward(self, x):
        output = self.conv1(x)
        output = self.batchNorm1(output)
        output = nn.ReLU()(output)
        output = self.resNetwork(output)
        # assert(output.shape == torch.Size([1, 256, 8, 8])) # This is only for batch = 1
        #print("passed body")
        return output

class policyHead(nn.Module):
    # AlphaZero: 1 rect, batch-norm conv layer (ASSUMED same parameters as nnBody)
    # AlphaZero: -> 73 filter conv layer (ASSUMED k = 1)
    # AlphaZero: output = 73 action space

    def __init__(self, nnBody = nnBody, planes = 73, stride = 1, kernel_size = 1, padding = 1, bias = False):
        super(policyHead, self).__init__()
        inputBody = nnBody()
        self.conv1 = nn.Conv2d(in_channels = inputBody.planes, out_channels = inputBody.planes, stride = inputBody.stride[0], kernel_size = inputBody.kernel_size[0], padding = inputBody.padding[0], bias = inputBody.bias)
        self.batchNorm1 = nn.BatchNorm2d(num_features = inputBody.planes)
        self.conv2 = nn.Conv2d(in_channels = inputBody.planes, out_channels = planes, stride = stride, kernel_size = kernel_size, bias = bias)
        self.batchNorm2 = nn.BatchNorm2d(num_features = planes)

    def forward(self, x):
        output = self.conv1(x)
        output = self.batchNorm1(output)
        output = nn.ReLU()(output)
        output = self.conv2(output)
        output = self.batchNorm2(output)
        output = nn.Softmax(dim = 1)(output)
        # assert(output.shape == torch.Size([1, 73, 8, 8])) # This is only for batch = 1
        #print("passed policy head")
        return output

class valueHead(nn.Module):
    # AlphaZero: 1 rect, batch-norm conv layer (k = 1 x 1, filtersize = 1, s = 1) 
    # AlphaZero: -> rectified linear layer of size 256 
    # AlphaZero: -> tanh-linear layer of size 1

    def __init__(self, nnBody = nnBody, linearLayer = 256, filterOut = 1, stride = 1, kernel_size = 1, padding = 0, bias = False):
        super(valueHead, self).__init__()
        inputBody = nnBody()
        boardSize = 64 # 8 x 8 chess board
        #print(inputBody.planes)
        #print("inputBody.planes")
        self.conv1 = nn.Conv2d(in_channels = inputBody.planes, out_channels = filterOut, kernel_size = kernel_size, stride = stride, padding = padding, bias = bias)
        self.batchNorm1 = nn.BatchNorm2d(num_features = filterOut)
        self.rectLinear = nn.Linear(in_features = boardSize, out_features = linearLayer) # 64 currently hardcoded
        self.tanhLinear = nn.Linear(in_features = linearLayer, out_features = 1)
   
    def forward(self, x):
        output = self.conv1(x)
        output = self.batchNorm1(output)
        output = nn.ReLU()(output)
        output = output.view(output.size(0), -1)
        output = self.rectLinear(output)
        output = nn.ReLU()(output)
        output = self.tanhLinear(output)
        output = nn.Tanh()(output)
        # assert(output.shape == torch.Size([1, 1])) # This is only for batch = 1
        #print("passed value head")
        return output


class LiaCha(nn.Module): #TODO Need to change assert checks for batch size = 1 to variable batch sizes
    # Introducing... LiaCha (Liang + Chang) model! 
    def __init__(self, **kwargs):
        super(LiaCha, self).__init__()

        # Initialize network architecture
        self.nnBody = nnBody()
        self.policyHead = policyHead()
        self.valueHead = valueHead()


        self.output_nnBody = None
        self.output_action = None
        self.output_prediction = None
        
        try:
            self.elo = kwargs["EloRating"] #might add back later
        except:
            self.elo = 0

    def forward(self, state):
        #print("Fwd Prop Initated:")
        # state = torch.from_numpy(state).float()
        # state = state.unsqueeze(0) # created artificial batch size = 1 for conv2d 
        self.output_nnBody = self.nnBody(state)
        self.output_action = self.policyHead(self.output_nnBody)
        self.output_prediction = self.valueHead(self.output_nnBody)
        #print("Fwd Prop complete...")
        return self.output_action, self.output_prediction


    # # TODO Incomplete
    # @staticmethod
    # def load(path: str):
    #     # params = torch.load(path, map_location=lambda storage, loc: storage)
    #     model = LiaCha()
    #     return model

    # # TODO Incomplete
    # def save(self, path: str):
    #     print('save model parameters to [%s]' % path, file=sys.stderr)
    #     # params = {
    #     # }
    #     # torch.save(params, path)
