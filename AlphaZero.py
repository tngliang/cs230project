#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""

from __future__ import print_function

import argparse
import datetime
from typing import List
import h5py
import math as math
import numpy as np
import os
import tensorflow as tf

import pdb

from AlphaZeroConfig import AlphaZeroConfig
from ChessGame import ChessGame
from ChessNetwork import ChessNetwork
from Game import Game
from Network import Network
from Node import Node
from ReadonlyChessGame import ReadonlyChessGame
from ReplayBuffer import ReplayBuffer
from SharedStorage import SharedStorage

##################################
####### Part 1: Self-Play ########

# Each self-play job is independent of all others; it takes the latest network
# snapshot, produces a game and makes it available to the training job by
# writing it to a shared replay buffer.
def run_selfplay(config: AlphaZeroConfig, storage: SharedStorage,
                 replay_buffer: ReplayBuffer):
  step = 0
  while True:
    #network = storage.latest_network()
    step, checkpoint = find_model(config, step)
    if not checkpoint is None:
      network = ChessNetwork(config, checkpoint, step)
    else:
      step = 0
      network = ChessNetwork(config)
    game = play_game(config, network)
    replay_buffer.save_game(game)


# Each game is produced by starting at the initial board position, then
# repeatedly executing a Monte Carlo Tree Search to generate moves until the end
# of the game is reached.
def play_game(config: AlphaZeroConfig, network: Network):
  game = ChessGame()
  while not game.terminal() and len(game.history) < config.max_moves:
    action, root = run_mcts(config, game, network)
    game.apply(action)
    game.store_search_statistics(root)
  return game

# Core Monte Carlo Tree Search algorithm.
# To decide on an action, we run N simulations, always starting at the root of
# the search tree and traversing the tree according to the UCB formula until we
# reach a leaf node.
def run_mcts(config: AlphaZeroConfig, game: Game, network: Network):
  root = Node(0)
  evaluate(root, game, network)
  add_exploration_noise(config, root)

  for _ in range(config.num_simulations):
    node = root
    scratch_game = game.clone()
    search_path = [node]

    while node.expanded():
      action, node = select_child(config, node)
      scratch_game.apply(action)
      search_path.append(node)

    value = evaluate(node, scratch_game, network)
    backpropagate(search_path, value, scratch_game.to_play())
  return select_action(config, game, root), root

def select_action(config: AlphaZeroConfig, game: Game, root: Node):
  visit_counts = [(child.visit_count, action)
                  for action, child in root.children.items()]
  if len(game.history) < config.num_sampling_moves:
    _, action = softmax_sample(visit_counts)
  else:
    _, action = max(visit_counts)
  return action

# Select the child with the highest UCB score.
def select_child(config: AlphaZeroConfig, node: Node):
  _, action, child = max((ucb_score(config, node, child), action, child)
                         for action, child in node.children.items())
  return action, child


# The score for a node is based on its value, plus an exploration bonus based on
# the prior.
def ucb_score(config: AlphaZeroConfig, parent: Node, child: Node):
  pb_c = math.log((parent.visit_count + config.pb_c_base + 1) /
                  config.pb_c_base) + config.pb_c_init
  pb_c *= math.sqrt(parent.visit_count) / (child.visit_count + 1)

  prior_score = pb_c * child.prior
  value_score = child.value()
  return prior_score + value_score


# We use the neural network to obtain a value and policy prediction.
def evaluate(node: Node, game: Game, network: Network):
  node.to_play = game.to_play()
  if game.terminal():
    return game.terminal_value(node.to_play)
  #print("evaluate {0} {1}".format(game.board.fen(), [m.uci() for m in game.board.move_stack]))
  images = []
  images.append(game.make_image(-1))
  masks = []
  masks.append(game.make_mask(-1))
  values, policy_logits = network.inference(images=images, masks=masks, training=False)

  # Expand the node.
  policy = {a: math.exp(policy_logits[0][a]) for a in game.legal_actions()}
  policy_sum = sum(policy.values())
  for action, p in policy.items():
    node.children[action] = Node(p / policy_sum)
  return values[0]

# At the end of a simulation, we propagate the evaluation all the way up the
# tree to the root.
def backpropagate(search_path: List[Node], value: float, to_play):
  for node in search_path:
    node.value_sum += value if node.to_play == to_play else -value
    node.visit_count += 1


# At the start of each search, we add dirichlet noise to the prior of the root
# to encourage the search to explore new actions.
def add_exploration_noise(config: AlphaZeroConfig, node: Node):
  actions = node.children.keys()
  noise = np.random.gamma(config.root_dirichlet_alpha, 1, len(actions))
  frac = config.root_exploration_fraction
  for a, n in zip(actions, noise):
    node.children[a].prior = node.children[a].prior * (1 - frac) + n * frac


######### End Self-Play ##########
##################################

##################################
####### Part 2: Training #########


def train_network(config: AlphaZeroConfig, storage: SharedStorage,
                  replay_buffer: ReplayBuffer):
  network = ChessNetwork(config)
  #optimizer = tf.train.MomentumOptimizer(config.learning_rate_schedule,
  #                                       config.momentum)
  for i in range(config.training_steps):
    if i % config.checkpoint_interval == 0:
      storage.save_network(i, network)
    batch = replay_buffer.sample_batch()
    #update_weights(optimizer, network, batch, config.weight_decay)
    update_weights(network, batch, config.weight_decay)
  storage.save_network(config.training_steps, network)


#def update_weights(optimizer: tf.train.Optimizer, network: Network, batch, weight_decay: float):
def update_weights(network: Network, batch, weight_decay: float):  
  #loss = 0
  #for image, (target_value, target_policy) in batch:
  #  value, policy_logits = network.inference(image)
  #  loss += (
  #      tf.losses.mean_squared_error(value, target_value) +
  #      tf.nn.softmax_cross_entropy_with_logits(
  #          logits=policy_logits, labels=target_policy))
  images, labels, masks = batch
  target_values, target_policies = zip(*labels)
  network.update_weights(images=images, masks=masks, target_values=np.transpose([target_values]), target_policies=target_policies)


######### End Training ###########
##################################

################################################################################
############################# End of pseudocode ################################
################################################################################

# Stubs to make the typechecker happy, should not be included in pseudocode
# for the paper.

def softmax_sample(visit_counts, percent=0.01, temperature=10):
  # AlphaZero paper softmax with temperature=10 to sample from actions with visit_counts less than 1% off from best visit_counts
  max_count, _ = max(visit_counts)
  threshhold = int(float(max_count) * (1 - percent))
  exp_cs = []
  choices = []
  total_exp_c = 0
  for i, (c, _) in enumerate(visit_counts):
    if c >= threshhold:
      exp_c = math.exp(float(c)/temperature)
      total_exp_c += exp_c
      exp_cs.append(exp_c)
      choices.append(i)
  return visit_counts[np.random.choice(choices, p = np.array(exp_cs) / total_exp_c)]

def launch_job(f, *args):
  pdb.set_trace()
  f(*args)

def self_replay_job(config: AlphaZeroConfig, storage: SharedStorage,
                  replay_buffer: ReplayBuffer):
  run_selfplay(config, storage, replay_buffer)

def evaluate_job():
  # evaluate the network against earlier versions of itself
  #tournament = ChessTournament(self.config.eval_num_games)
  #tournament.Run(step, ChessEngine(self.latest_network()), ChessEngine(network), self.config)
  pass

def predict(step: int, network: Network, heldout_set: ReplayBuffer, config: AlphaZeroConfig):
  avg_loss = 0
  i = 0
  for batch in heldout_set.sample_moves():
    images, labels, masks = batch
    target_values, target_policies = zip(*labels)
    loss = network.predict(images, masks, np.transpose([target_values]), target_policies, training=False)
    avg_loss = avg_loss * (i / (i + 1)) + loss / (i + 1)
    i += 1
  return avg_loss

def predict_batch(step: int, network: Network, batch, config: AlphaZeroConfig):
  images, labels, masks = batch
  target_values, target_policies = zip(*labels)
  _, _, loss, grad_norm = network.predict(images, masks, np.transpose([target_values]), target_policies, training=False)
  return (loss, grad_norm)

def find_model(config: AlphaZeroConfig, required_step: int):
  modelDir = config.checkpoint_output_path
  checkpoint = None
  for item in os.listdir(modelDir):
    filePath = os.path.join(modelDir, item)
    if os.path.isfile(filePath) and item.endswith(".meta"):
      ckpt_index = item.find(".ckpt")
      dash_index = item.find('-')
      meta_index = item.find(".meta")
      step = int(item[dash_index+1:meta_index])
      if step >= required_step:
        required_step = step
        checkpoint = item[0:ckpt_index]
  return (required_step, checkpoint)

def main(args):
  config = AlphaZeroConfig()
  if not os.path.exists(config.output_path):
    os.makedirs(config.output_path)
  if not os.path.exists(config.checkpoint_output_path):
    os.makedirs(config.checkpoint_output_path)
  if not os.path.exists(config.game_output_path):
    os.makedirs(config.game_output_path)
  if not os.path.exists(config.log_path):
    os.makedirs(config.log_path)

  if args.selfplay:
    self_replay_job(config, SharedStorage(config), ReplayBuffer(config))
  elif args.selfplaytrain:
    train_network(config, SharedStorage(config), ReplayBuffer(config))
  elif not args.load is None:
    pgnDir = args.load
    gamesDir = config.game_output_path
    if not os.path.exists(gamesDir):
      os.makedirs(gamesDir)
    if args.index is None:
      i = 0
    else:
      i = int(args.index)
    if args.suffix is None:
      super_batch = 0
      ext = ".pgn"
    else:
      super_batch = int(args.suffix)
      ext = "{:02d}.pgn".format(super_batch)
    for item in os.listdir(pgnDir):
      filePath = os.path.join(pgnDir, item)
      if os.path.isfile(filePath) and item.endswith(ext):
        batch = super_batch * config.max_games_to_load + int(i / config.max_games_to_load)
        try:
          h5py_file = h5py.File(os.path.join(gamesDir, "{:s}.{:05d}.h5".format(item, batch)), 'w')
          h5py_games = h5py_file.create_group("games")
          for game in ChessGame.loadpgn(filePath, i, config.max_games_to_load):
            print("{0} loaded games {1} ...".format(datetime.datetime.now(), i))
            if not game is None:
              game.saveh5py(h5py_games.create_group(str(i)))
              i += 1
        finally:
          h5py_file.close()
  elif args.train:
    devDir = os.path.join(config.data_path, "dev/")
    if args.index is None:
      i = 0
    else:
      i = int(args.index)
    k = 0
    # load saved network
    # sequentially put games into replay buffer and train network
    step = 0
    storage = SharedStorage(config)
    step, checkpoint = find_model(config, step)
    if not checkpoint is None:
      network = ChessNetwork(config, checkpoint, step)
    else:
      step = 0
      network = ChessNetwork(config)
    # sequentially put dev games into replay buffer to validate network parameters
    for item in os.listdir(devDir):
      filePath = os.path.join(devDir, item)
      if os.path.isfile(filePath) and item.endswith(".h5"):
        if k == i:
          print("{0} loading dev-set games {1} - {2} ...".format(datetime.datetime.now(), i, i + config.max_games_to_load))
          dev_set = ReplayBuffer(config)
          try:
            h5py_file = h5py.File(filePath, "r")
            h5py_games = h5py_file['games']
            for key in h5py_games.keys():
              game = ReadonlyChessGame(h5py_games[key])
              dev_set.save_game(game)
          finally:
            h5py_file.close()
        k += 1
    # sequentially put train games into replay buffer and train network
    trainDir = os.path.join(config.data_path, "train/")
    for item in os.listdir(trainDir):
      filePath = os.path.join(trainDir, item)
      if os.path.isfile(filePath) and item.endswith(".h5"):
        print("{0} loading training games {1} - {2} for step {3} ...".format(datetime.datetime.now(), step * config.max_games_to_load, (step + 1) * config.max_games_to_load, step))
        train_set = ReplayBuffer(config)
        try:
          h5py_file = h5py.File(filePath, "r")
          h5py_games = h5py_file['games']
          for key in h5py_games.keys():
            game = ReadonlyChessGame(h5py_games[key])
            train_set.save_game(game)
        finally:
          h5py_file.close()
        print("{0} training on games {1} - {2} for step {3} ...".format(datetime.datetime.now(), step * config.max_games_to_load, (step + 1) * config.max_games_to_load, step))
        for _ in range(config.num_steps_per_games):
          train_batch = train_set.sample_batch()
          dev_batch = dev_set.sample_batch()
          update_weights(network, train_batch, config.weight_decay)
          storage.save_network(step, network)
          train_loss, train_grad_norm = predict_batch(step, network, train_batch, config)
          dev_loss, _ = predict_batch(step, network, dev_batch, config)
          network.record_summary(step, [train_loss, dev_loss, np.nan, train_grad_norm], config)
          step += 1
        #for batch in train_set.sample_moves():
        #  update_weights(network, batch, config.weight_decay)
        #storage.save_network(step, network)
        #train_loss = predict(step, network, train_set, config)
        #dev_loss = predict(step, network, dev_set, config)
        #network.record_summary(step, [train_loss, dev_loss, np.nan, grad_norm], config)
        #repeat = True
        #while repeat:
        #  train_batch = train_set.sample_batch()
        #  dev_batch = dev_set.sample_batch()
        #  for k in range(100000):
        #    #images, labels, masks = train_batch
        #    #target_values, target_policies = zip(*labels)
        #    update_weights(network, train_batch, config.weight_decay)
        #    train_loss, train_grad_norm = predict_batch(step, network, train_batch, config)
        #    #values, policy_logits = network.inference(images=images, masks=masks)
        #    #policy = np.exp(policy_logits)
        #    #policy_sum = np.sum(policy)
        #    #policy_softmax = policy / policy_sum
        #    #print("v={0} v_hat={1} a={2} a_hat={3}".format(target_values, values, np.argmax(target_policies), np.argmax(policy_softmax)))
        #    #print("policy_softmax={0}".format(np.argmax(policy_softmax)))
        #    dev_loss, _ = predict_batch(step, network, dev_batch, config)
        #    network.record_summary(step, [train_loss, dev_loss, np.nan, train_grad_norm], config)
        #    storage.save_network(step, network)
        #    step += 1
        #  if step % 10 == 0:
        #    repeat = False
        
        #batch = train_set.sample_batch()
        #while step < 700e3:
        #  update_weights(network, batch, config.weight_decay)
        #  storage.save_network(step, network)
        #  train_loss = predict_batch(step, network, batch, config)
        #  network.record_summary(step, [train_loss, np.nan, np.nan], config)
        #  step += 1
    # sequentially put test games into replay buffer and test trained network
    testDir = os.path.join(config.data_path, "test/")
    if args.index is None:
      i = 0
    else:
      i = int(args.index)
    k = 0
    for item in os.listdir(testDir):
      filePath = os.path.join(testDir, item)
      if os.path.isfile(filePath) and item.endswith(".h5"):
        if k == i:
          print("{0} loading test-set games {1} - {2} ...".format(datetime.datetime.now(), i, i + config.max_games_to_load))
          test_set = ReplayBuffer(config)
          try:
            h5py_file = h5py.File(filePath, "r")
            h5py_games = h5py_file['games']
            for key in h5py_games.keys():
              game = ReadonlyChessGame(h5py_games[key])
              test_set.save_game(game)
          finally:
            h5py_file.close()
        k += 1
    train_loss, train_grad_norm = predict_batch(step, network, train_set.sample_batch(), config)
    dev_loss, _ = predict_batch(step, network, dev_set.sample_batch(), config)
    test_loss, _ = predict_batch(step, network, test_set.sample_batch(), config)
    network.record_summary(step, [train_loss, dev_loss, test_loss, train_grad_norm], config)


if __name__ == '__main__':
  parser = argparse.ArgumentParser('AlphaZero selfplay trainer converter')
  parser.add_argument('-cpu', '--cpu', action='store_true', help='run in cpu only mode')
  parser.add_argument('-s', '--selfplay', action='store_true', help='generate new self-play games')
  parser.add_argument('-t', '--train', action='store_true', help='train against recorded games')
  parser.add_argument('-l', '--load', help='load pgn files and persist them to cache')
  parser.add_argument('-i', '--index', help='starting index of games in pgn files to start loading')
  parser.add_argument('-x', '--suffix', help='suffix of pgn files to loading')
  parser.add_argument('-a', '--selfplaytrain', action='store_true', help='train against self-play games')
  args = parser.parse_args()
  main(args)