#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""

from __future__ import print_function

import numpy as np
import collections

import datetime

from AlphaZeroConfig import AlphaZeroConfig
from Game import Game

import traceback
import pdb

n = 8

class ReadonlyChessGame(Game):
    def terminal(self):
        return self.terminated

    def terminal_value(self, to_play):
        # Game specific value.
        return self.getScore(to_play)

    def legal_actions(self):
        raise Exception("Not Implemented")

    def clone(self):
        raise Exception("Not Implemented")

    def apply(self, action, store: bool = True):
        super().apply(action) # commit to super class history also
        
    def store_search_statistics(self, root):
        sum_visits = sum(child.visit_count for child in root.children.values())
        #self.child_visits.append([
        #    root.children[a].visit_count / sum_visits if a in root.children else 0
        #    for a in np.ndindex(self.getActionSize())
        #])
        child_visits = np.zeros(self.getActionSize())
        for a in root.children:
            child_visits[a] = root.children[a].visit_count / sum_visits
        self.child_visits.append(child_visits)

    def make_image(self, state_index: int):
        return self.states[state_index]

    def make_mask(self, state_index: int):
        return self.legals[state_index]

    def make_target(self, state_index: int):
        target_value = self.terminal_value(1 - state_index % 2)
        if state_index < len(self.child_visits):
            target_policy = self.child_visits[state_index]
        else:
            target_policy = np.zeros(self.getActionSize())
            target_policy[self.history[state_index]] = 1 # one-hot historic action as target (for supervised-learning)
        return (target_value, target_policy)

    def to_play(self):
        return 1 - len(self.history) % 2 # 1 = white, 0 = black

    def saveh5py(self, h5py_group):
        h5py_group.create_dataset("states", data=self.states, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("history", data=self.history, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("legals", data=self.legals, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("child_visits", data=self.child_visits, compression='gzip', compression_opts=9)
        h5py_group.create_dataset("terminated", data=self.terminated)
        h5py_group.create_dataset("result", data=self.result)
        h5py_group.create_dataset("score", data=self.score)
        h5py_group.create_dataset("whiteScore", data=self.whiteScore)

    def __init__(self, h5py_game):
        self.states = h5py_game["states"][()]
        self.history = h5py_game["history"][()]
        self.legals = h5py_game["legals"][()]
        self.child_visits = h5py_game["child_visits"][()]
        self.terminated = h5py_game["terminated"][()]
        self.result = h5py_game["result"][()]
        self.score = h5py_game["score"][()]
        self.whiteScore = h5py_game["whiteScore"][()]
    
    def getBoardSize(self):
        return (119, n, n)

    def getActionSize(self):
        return (73, n, n)

    def toString(self):
        raise Exception("Not Implemented")

    def getScore(self, to_play):
        return -self.score if self.to_play() == to_play else self.score
        
    def getWhiteScore(self):
        return self.whiteScore

if __name__=="__main__":
    pass
