#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS230 2019-Winter - Final Project: Recreating AlphaZero
Mark Chang <mrkchang@stanford.edu>
Ting Liang <tngliang@stanford.edu>
"""
from __future__ import print_function

from AlphaZero import run_mcts
from AlphaZeroConfig import AlphaZeroConfig
from ChessEngine import ChessEngine
from ChessGame import ChessGame
from ChessGame import uciActions
from ChessNetwork import ChessNetwork
from MCTS import MCTS
from LiaCha_model import LiaCha
from ChessGame import actions as actionsMAP

class LiaChaEngine(ChessEngine):
    def __init__(self, config: AlphaZeroConfig, network: LiaCha, thinkingTime = 200, name = "LiaCha"):
        self.name = name
        self.config = config
        self.config.num_sampling_moves = 0 # do not sample during evaluation
        self.network = network
        self.thinkingTime = thinkingTime
        super().__init__(self.network.elo) # default for chess

    def play(self, game: ChessGame):
        
        if not game.getGameEnded():
            tree = MCTS(game, self.network)
            action, _ = tree.recommendBestMove(game.board, simulations = self.thinkingTime)
            # print("LiaCha played {0}".format(action))

            action = actionsMAP[action]
            game.apply(action)
            


    # 
    # move = tree.recommendBestMove(board)
