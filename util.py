from ChessGame import ChessGame, uciActions, actions
import pdb
import chess
import torch 
import argparse
import os

from torch.utils import data
import random
import numpy as np

class Dataset(data.Dataset):
  'Characterizes a dataset for PyTorch'
  def __init__(self, moves, states, results):
        'Initialization'
        self.moves = moves
        self.states = states
        self.results = results

  def __len__(self):
        'Denotes the total number of samples'
        return self.moves.size()[0]

  def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        move = self.moves[index]
        state = self.states[index,:,:,:]
        result = self.results[index]

        # Load data and get label
        # X = torch.load('data/' + ID + '.pt') # This is cool if you want to load files.

        return move, state, result

def generateexample(num_games):
    pgn_file = '.\data\ChessTournament01.pgn'
    moves, states, results = supervised_pgn_read(pgn_file, num_games = num_games)
    torch.save((moves, states, results), f = './data/train.pt')      


def generateexample_stockfish(num_games):
    moves, states, results = playstockfish(games = 500, simulations = 20)
    torch.save((moves, states, results), f = './data/train.pt')      


def playstockfish(games = 500, simulations = 20):
    import chess.engine
    engine = chess.engine.SimpleEngine.popen_uci("Stockfish_10_x64")

    moves = []
    states = []
    results = []

    examples = 0

    for _ in range(games):
        chessobj = ChessGame(chess.Board(chess960=False))
        board_main = chess.Board(chess960=False)
        movecount = 0
        # while not board_main.is_game_over():
        while not chessobj.board.is_game_over():

            state = chessobj.boardToState()
            state = torch.tensor(state, dtype = torch.float)
            states.append(state)
            
            moveProb = torch.zeros([73, 8, 8], dtype = torch.float)
            
            # note: seems like we don't actually need to create sims
            # board_sim = chess.Board(fen = board_main.fen())
            board_sim = chess.Board(fen = chessobj.board.fen())
            for _ in range(simulations):
                # result = engine.play(chessobj.board, chess.engine.Limit(time=None))
                result = engine.play(board_sim, chess.engine.Limit(time=None))
                plane, x0, y0 = actions[result.move.uci()]
                moveProb[plane, x0, y0] += 1

                # print(result.move)
                # print(uciActions[plane, y0, x0])
                # board_sim.push.from_uci(result.move)        
                board_sim = chess.Board(fen = chessobj.board.fen())
            
            
            actionProbs = moveProb.cpu().numpy()
            (plane, x0, y0) = np.unravel_index(np.argmax(actionProbs), actionProbs.shape)
            # print(chessobj.board)
            # print(uciActions[plane, y0, x0])
            # pdb.set_trace()
            chessobj.board.push(chess.Move.from_uci(uciActions[plane, y0, x0]))
            movecount += 1

            moveProb /= torch.sum(moveProb)
            moveProb = moveProb.view(-1)
            moves.append(moveProb)
        
        result = interpretResult(board_main.result(claim_draw=True)) # TODO: delete util part
        result = torch.tensor([result], dtype = torch.float)
        for _ in range(movecount):
            results.append(result)
        
        examples +=movecount
        if examples > int(200e3):
            break #for memory

    moves, states, results = torch.stack(moves), torch.stack(states), torch.stack(results)
    return moves, states, results


def supervised_pgn_read(pgn_file, num_games = 1000):
    # examples = [] # This will be list of moves and boards
    pgn = open(pgn_file, encoding="utf-8")
    pgn_game = chess.pgn.read_game(pgn)
    
    print("Processing: " + str(num_games) + " games...")
    gamecount = 0

    # moves = torch.zeros([1, 73, 8, 8], dtype = torch.float) 
    # states = torch.zeros([1, 119, 8, 8], dtype = torch.float)
    # results = torch.zeros([1], dtype = torch.float)
    moves = []
    states = []
    results = []

    while not pgn_game is None:
        # initialize shape. Will take out first dim later.
        # print("game count: " + str(gamecount))

        # get result
        result = pgn_game.headers['Result']
        result = interpretResult(result)
        print("this game")
        print(pgn_game.headers['Round'])
            
            
        board = pgn_game.board()
        chessGameObject = ChessGame(board)
        
        count =0
        for move in pgn_game.mainline_moves():
            
            if board.is_capture(move) or result == 0 or count <= 5 or gamecount <= 2000:
                print("draw or move is capture or count <=5 - ignored")
                print("also could be iterating next game count - ignored")
            else:
                # save moves
                plane, x0, y0 = actions[move.uci()]
                oneHot = torch.zeros([73, 8, 8], dtype = torch.float)
                oneHot[plane][x0][y0] = 1
                oneHot = oneHot.view(-1) # as 1 hot location
                oneHot = torch.argmax(oneHot)
                moves.append(oneHot)

                # save states
                state = chessGameObject.boardToState()
                state = torch.tensor(state, dtype = torch.float)
                states.append(state)

                # save results
                result = torch.tensor([result], dtype = torch.float)
                results.append(result)

            # push board
            board.push(move)
            count+=1

        # informational purpose only
        # print("move count/game: " + str(count))
        # print("result/game: " + str(result))

        # read next game
        pgn_game = chess.pgn.read_game(pgn)
        gamecount += 1
        if gamecount % 100 == 0:
            print("game count: " + str(gamecount))
        
        if gamecount >= num_games + 2000:
            break
    
    # moves, states, results = moves[1:,:,:,:], states[1:,:,:,:], torch.unsqueeze(results[1:], dim = 1)
    print("Completed processing " + str(num_games) + " games...")
    moves, states, results = torch.stack(moves), torch.stack(states), torch.stack(results)
    return moves, states, results

def interpretResult(pgnresult):
    if pgnresult == '1-0':
        return 1  # white win
    elif pgnresult == '0-1':
        return -1
    else:
        return 0

def get_available_devices():
    """Get IDs of all available GPUs.

    Returns:
        device (torch.device): Main device (GPU 0 or CPU).
        gpu_ids (list): List of IDs of all GPUs that are available.
    """
    gpu_ids = []
    if torch.cuda.is_available():
        gpu_ids += [gpu_id for gpu_id in range(torch.cuda.device_count())]
        device = torch.device('cuda:{}'.format(gpu_ids[0]))
        torch.cuda.set_device(device)
    else:
        device = torch.device('cpu')

    return device, gpu_ids
# pgn_file = '.\data\ChessTournament01.pgn'
# moves, boards, results = supervised_pgn_read(pgn_file, num_games = 10)        

def get_save_dir(base_dir, id_max=100):
    """Get a unique save directory by appending the smallest positive integer
    `id < id_max` that is not already taken (i.e., no dir exists with that id).

    Args:
        base_dir (str): Base directory in which to make save directories.
        name (str): Name to identify this training run. Need not be unique.
        training (bool): Save dir. is for training (determines subdirectory).
        id_max (int): Maximum ID number before raising an exception.

    Returns:
        save_dir (str): Path to a new directory with a unique name.
    """
    for uid in range(1, id_max):
        save_dir = os.path.join(base_dir, 'tboard-{:02d}'.format(uid))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
            return save_dir

    raise RuntimeError('Too many save directories created with the same name. \
                       Delete old save directories or use another name.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--generate", action='store_true', help="Whether to run training.")
    # parser.add_argument("--model", action='store_true', help="Whether to run training.")
    # parser.add_argument("--do_predict", action='store_true', help="Whether to run eval on the dev set.")
    parser.add_argument("--count", default=None, type=int, required=False,help="")
    parser.add_argument("--gstockfish", action='store_true', help="Whether to run training.")

    args = parser.parse_args()
    if args.gstockfish:
        if args.count is not None:
            count = args.count
        else:
            count = 1000
        generateexample_stockfish(count)

    if args.generate:
        if args.count is not None:
            
            count = args.count
        else:
            count = 1000
        print("#games being generated: " +str(count))
        generateexample(count)